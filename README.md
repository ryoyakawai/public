[Direcotry of WebAudioAPI]
==========================

Sample01
========

概要
----
AudioBufferSourceNode Interface に [dsp.js] を使って作成した、サイン波、三角波、ノコギリ波波形を確認できるように波形の表示も行います。

実行時画面イメージ
-----------------
[sample01実行時イメージ]

ファイルの説明
-------------
 * *index.html*
 * [*js*]
   * *canvas07.js* : 波形描画スクリプト(一部)
   * *dsp.js* : dsp.js 本体
   * *jquery.min.js* : jQuery 本体
   * *oscillator.js* : キー入力を判別し dsp.js にて波形を作成し、音声を出すスクリプト、波形描画スクリプト（一部）
   * *oscillator_button.js* : ブラウザボタンの操作を処理するスクリプト
 * [*images*]
   * *keyAssign.jpg* : 鍵盤の画像

メモ
----
 * 2012/5/29に和音対応しました。(単音しか出ません)
 * 1オクターブしか定義していない為、1オクターブしか出ません。定義をすれば発声可能です。
 * dsp.js L975辺りがバグっぽかったので、修正しています。
   * [修正前] *switch(parseInt(type, 10)) {*
   * [修正後] *switch(type) {*
 * 「ブツ、ブツ」と雑音が入ります。(波形を作っている dsp.js に問題がありそう)
   * (2012/04/26:修正済:v1.0)原因はbufferサイズ、キーボード押しっ放し時のキー入力の特性の2点でした。キーボード入力は1度入力を検出した場合、キーを離すまで入力を受け付けないよう変更を加えています。



[sample01実行時イメージ]: https://bitbucket.org/ryoyakawai/public/raw/9ff7a0ead118/README.image/sample01_image.jpg "sample01実行時イメージ"
[dsp.js]: https://github.com/corbanbrook/dsp.js/ "dsp.js"



---

Sample02
========

概要
----
XMLHttpRequest を使って BufferSource にオーディオファイルを格納して再生します。波形の表示も行います。

ファイルの説明
-------------
 * *index.html*
 * [*js*]
   * *canvas07.js* : 波形描画スクリプト(一部)
   * *jquery.min.js* : jQuery 本体
   * *getKeycodefromKeyboard.js* : キーボードの入力を処理するスクリプト
   * *soundSource.js* : 音源定義ファイル
   * *windowEvent.js* : ブラウザボタンの入力を処理するスクリプト
   * *xhrjsTone.js* : 音源のLoad、発音処理をするスクリプト
 * [*sounds*]
   * 音源ファイル格納ディレクトリ

メモ
----
 特になし。


Sample03
========
（作成中です）

---

Sample04
========

概要
----
WebAudio の Panner の効果を [Three.js] (WebGL) を使って分かりやすく表現しています。

実行時の注意
---------
そこそこ動作が重いので、実行直前に作業中のアプリケーションのファイルを保存することをおすすめします。

ファイルの説明
-------------
 * *index.html*
 * [*js*]
   * *Three.js* : Three.js 本体
   * *jquery.min.js* : jQuery 本体
   * *threeRenderMouseEvent.js : マウスイベントに関するスクリプト
   * *windowClickEvent.js : ボタン操作に関するスクリプト
   * *xhrSound.js : 音源ファイルをロード
 * [*sounds*]
   * 音源ファイル格納ディレクトリ

メモ
----
 * 床を表現する平面の回転ではなく、強引にカメラの位置をずらすことで目的空間を表現している為、球体を動かす座標と音の発信源の座標の差を埋める為にコード量が増えています。
  * plane.rotation.x = -1 * Math.PI/2; 等することで回転可能


[Three.js]: http://mrdoob.github.com/three.js/

---

Sample05
========

概要
----
HTML5 の FileReader と WebAudio を組み合わせ、ファイルドロップと同時に再生されます。

実行時の制限
---------
再生はWAVE ファイルのみに限っています。

ファイルの説明
------------
 * *index.html*
 * [*js*]
   * *jquery.min.js* : jQuery 本体
   * *drag_n_drop.js* : Drag&Drop時の動作に関するスクリプト
   * *windowClickEvent.js : ボタン操作に関するスクリプト
 * [*css*]
   * drag_n_drop.css : Drop領域のデザイン定義


メモ
----
 特になし

---

Sample06
========

概要
----
HTML5 の FileReader と WebAudio を組み合わせ、ファイルドロップと同時に再生されます。
複数の音源ファイルドロップしてミキシングすることができます。
同時に複数の音源ファイルドロップすることも可能です。

実行時の制限
---------
audio/* のタイプがドロップ可能です。
（ただし、確認しているのはmp3とwaveのみになります。）

ファイルの説明
------------
 * *index.html*
 * [*js*]
   * *jquery.min.js* : jQuery 本体
   * *drag_n_drop2.js* : Drag&Drop時の動作に関するスクリプト
   * *track.js* : 各トラックに音源ファイルを割当てるスクリプト
   * *track_element.js* : トラックのHTMLソース
   * *windowClickEvent.js : ボタン操作に関するスクリプト
 * [*css*]
   * drag_n_drop.css : Drop領域のデザイン定義


メモ
----
 特になし

---

Sample06-01
===========

概要
----
Sample06にスペクトルアナライザ、イコライザを追加


実行時の制限
-----------
Sample06と同様

ファイルの説明
------------
Sample06と同様


メモ
----
 特になし


---

Sample06-02
===========

概要
----
Sample06-01 にドロップしたファイルの波形表示、再生位置決め、再生位置表示機能を追加


実行時の制限
-----------
Sample06と同様

ファイルの説明
------------
Sample06と同様


メモ
----
 特になし


---

---


[Direcotry of WebRTC]
==========================

Sample01
========

概要
----
getUserMediaを使う簡単なデモ。
画像クリックでCSS3のFilterを追加することも可能。

実行時の制限
---------
実行するコンピュータにカメラが接続されていること。

ファイルの説明
------------
 (まとめ中)

メモ
----
 特になし

---

Sample02
========

概要
----
getUserMediaを使う簡単なデモ。
スナップショット時にCSS3のFilterをランラムに追加する。

実行時の制限
---------
実行するコンピュータにカメラが接続されていること。

ファイルの説明
------------
 (まとめ中)

メモ
----
 特になし

---

Sample03
========

概要
----
getUserMediaを使う簡単なデモ。
映像と音声をリアルタイムに取得して、映像は表示、音声は再生しならば
WebAudioAPIのAnalyserの機能を使ってスペクトルアナライザを表示。

実行時の制限
---------
実行するコンピュータにカメラが接続されていること。

ファイルの説明
------------
 (まとめ中)

メモ
----
 特になし
