
var canvas07 = function() {
  return {
    init: function(context, canvas_context) {
      this.analyser = context.createAnalyser();
      this.timeDomainData = new Uint8Array(this.analyser.frequencyBinCount);
      this.canvas_context = canvas_context;
    },
    
  }
}();



//set requestAnimationFrame to window (with vendor prefixes)
(function (w, r){
    w['r'+r] = w['r'+r] || w['webkitR'+r] || w['mozR'+r] || w['msR'+r] || w['oR'+r] || function(c){ w.setTimeout(c, 1000 / 60); };
})(window, 'equestAnimationFrame');

