var socket = null;
var wsConnection = false;

function createWebSocket(wsIp, wsPort) {
  
  // Socketの初期化
  socket = new WebSocket('ws://' + wsIp + ':' + wsPort + '/service');
  socket.binaryType = 'arraybuffer';

  socket.onopen=wsOpen;
  socket.onclose=wsClose;
  socket.onerror=wsError;
  socket.onmessage = handleReceive;

};

function closeWebSocket(wsIp, wsPort) {
  socket.onclose=null;
  socket.close();
};


var noteOn = [];

function handleReceive(message) {
  if( typeof message.data == 'string' ) {
    console.log("message data is string");
    console.log("messgae: %s", message.data );
  } else {
    var buffer = new Uint8Array(message.data);
    
    if(buffer.length != 3 ) {
      console.log("data length not 3");
      return;
    }
    var int1 = buffer[0];
    var int2 = buffer[1];
    var int3 = buffer[2];
    
    var num1 = new Number(buffer[0]);
    var num2 = new Number(buffer[1]);
    var num3 = new Number(buffer[2]);
    var str1 = num1.toString(16);
    var str2 = num2.toString(16);
    var str3 = num3.toString(16);

    if( str1.length < 2 ) str1 = "0" + str1;
    if( str2.length < 2 ) str2 = "0" + str2;
    if( str3.length < 2 ) str3 = "0" + str3;

    //console.log( int1, int2, int3 );    
    if( str1 == "90" && int3>0 ) {
//      if( NoteOn[int2] ) {
//        return;
//      }
      noteOn[int2] = true;
      int2_wav = 'midi_' + int2;
      dspjsTone.noteOn( int2, int3 );
    } else if ( str1 == "80" || str1 =="90" ) {
      dspjsTone.noteOff( int2 );
      noteOn[int2] = false;
    } else {
      console.log("num1 is not 80 nor 90");
    }
  }
}

