window.document.onkeyup = function(e){
  var keyCode=e.keyCode;
  dspjsTone.noteOff(keyCodeMap[keyCode]);
};
window.document.onkeydown = function(e){
  if( dspjsTone.noteOnFlag == true ) return;
  var keyCode=e.keyCode;
  dspjsTone.noteOn(keyCodeMap[keyCode]);    
};

var  keyCodeMap = {
// PCKeyCode: PianoKeyNo
  90: 69,
  83: 70,
  88: 71,
  67: 72,
  70: 73,
  86: 74,
  71: 75,
  66: 76,
  78: 77,
  74: 78,
  77: 79,
  75: 80
};

$(function(){
});

var dspjsTone = function() {
  
  // creating DoReMi Freq
  var r = Math.pow( 2.0, 1.0 / 12.0 );
  var noteDefMIDI = new Array();
  noteDefMIDI[69] = 440 ;
  for ( var i = 70; i <= 127; i++ ) {
    noteDefMIDI[i] = noteDefMIDI[i-1] * r;
  };
  for (  i = 68; i >= 0; i-- ) {
    noteDefMIDI[i] = noteDefMIDI[i+1] / r;
  };
  
  var OSCTYPE = {
    'sine'    : 0,
    'square'  : 1,
    'saw'     : 2,
    'triangle': 3,
    'custom'  : 4
  };

    return {
    init: function(context, canvas_context){ 
      this.context = context;
      this.pannerNode = this.context.createPanner();
      this.pannerPosition = { "x": 0, "y": 0, "z": 0 };
      this.presetStartDelay = 0;
      this.src = this.context.createBufferSource();
      this.src = new Array();
      this.osc = new Array();
      this.noteTone = new Array();
      this.bufsize = new Array();
      this.sampleRate = 44100;
      this.amplitude = '0';
      this.wave_type = 'DSP.SINE';
      this.oscType='sine';
      this.canvas_context = canvas_context;
      this.lastkeyCode=null;
      this.noteOnFlag = new Array();
      for(var i = 0; i <= 127; i++ )　{
        this.noteOnFlag[i]=false;
      }

    },

    setOscType: function(type) {
      this.oscType = type;
    },

    setAmplitude: function(num) {
      this.amplitude = num;
    },

    setSampleRate: function(num) {
      this.sampleRate = num;
    },

    setPannerNode: function(x, y, z) {
      if(typeof(x) == "") x = 0;
      if(typeof(y) == "") y = 0;
      if(typeof(z) == "") z = 0;

      this.pannerPosition = { "x": x, "y": y, "z": z };
      this.pannerNode.setPosition(x, y, z);
    },

    noteOn: function(note) {
      if(this.noteOnFlag[note]==false) {
        var note_freq = noteDefMIDI[note];
        this.osc[note] = this.context.createOscillator();

        this.osc[note].type = OSCTYPE[this.oscType];
        this.osc[note].frequency.value = note_freq;

        switch( OSCTYPE[this.oscType] ) {
        case 4:
          var coeffA = new Float32Array([0, 1, 0.5]);
          var coeffB = new Float32Array([0, 0, 0]);
          var wavetable = this.context.createWaveTable(coeffA, coeffB);
          this.osc[note].setWaveTable(wavetable);
          break;          
        default:
          this.osc[note].frequency.value = note_freq;
          break;
        }

        this.noteOnFlag[note]=true;

        this.pannerNode.setPosition(this.pannerPosition.x, this.pannerPosition.y, this.pannerPosition.z);
        this.gainNode = this.context.createGainNode();
        this.gainNode.gain.value = this.amplitude;

        this.osc[note].connect(this.gainNode);
        this.gainNode.connect(this.pannerNode);
        this.pannerNode.connect(this.context.destination);

        // canvas
        this.gainNode.connect(canvas07.analyser);
        animationLoopTime();
        // canvas
      }
    },

    noteOff: function(note) {
      if(note=='') note=0;
      this.noteOnFlag[note]=false;
      this.osc[note].disconnect();
    }
  }
}();

// canvas
function render(ele, ctx, data){
  var ctx01 = ctx;
  
  //背景
  ctx01.beginPath();
  ctx01.fillStyle = 'black';
  ctx01.rect(0, 0, ele.width, ele.height);
  ctx01.closePath();
  ctx01.fill();
  //基準線
  ctx01.beginPath();
  ctx01.strokeStyle = 'red';
  ctx01.moveTo(0, ele.height/2);
  ctx01.lineTo(ele.width, ele.height/2);
  ctx01.closePath();
  ctx01.stroke();
  //波形
  var value;
  ctx01.beginPath();
  ctx01.strokeStyle = 'gray';
  ctx01.moveTo(0,-999);
  for (var i = 0; i < data.length; ++i){
				    value = (parseInt(data[i]) - 128) / 2 + ele.height / 2;
				ctx01.lineTo(i,value);
  }
  ctx01.moveTo(0,999);
  ctx01.closePath();
  ctx01.stroke();
}

var cnt=0;
function animationLoopTime(){
  cnt++;
  if(cnt%2){
    canvas07.analyser.getByteTimeDomainData(canvas07.timeDomainData);
    //analyser.getByteFrequencyData(timeDomainData);
    render(canvas01, canvas07.canvas_context, canvas07.timeDomainData);
  }
  requestAnimationFrame(animationLoopTime);
}



