$(function(){

  try {
    var context = new webkitAudioContext();
  } catch(e) {
		  alert('webkitAudioContext()をサポートしているブラウザでお試しください。(例:chrome)');
  }

  var canvas01 = document.getElementById('canvas01');
  var canvas01_context = canvas01.getContext('2d');
  dspjsTone.init(context, canvas01_context);
  canvas07.init(context, canvas01_context);
  
  // init ui
  dspjsTone.setAmplitude($('input.range_amplitude').val()/20);
  $('input.text_range_amplitude').attr('value', dspjsTone.amplitude);

  //$('input.OscillatorType').attr('input[name=oscType]', 'checked');
  $('input.OscillatorTypeShow').attr('value', $('input[name=oscType]:checked').val());
  $("input[name='samplingRate']").val(['44100']);
  if( window.navigator.platform.indexOf('Mac')==0 ) {
    $("input[name='samplingRate']").val(['48000']);
  }  
  // init ui

  $('#OscillatorType').change(function(){
    dspjsTone.setOscType($('input[name=oscType]:checked').val());
    $('input.OscillatorTypeShow').attr('value', dspjsTone.oscType);
  });

  $('input.range_amplitude').change(function(){
    dspjsTone.setAmplitude($('input.range_amplitude').val()/20);
    $('input.text_range_amplitude').attr('value', dspjsTone.amplitude);
  });

  //PannerNode
  $('input.range_pan_x').change(function(){
    var pan_x_value = $('input.range_pan_x').val();
    var pan_y_value = $('input.range_pan_y').val();
    var pan_z_value = $('input.range_pan_z').val();
    dspjsTone.setPannerNode(pan_x_value, pan_y_value, pan_z_value );
    $('input.text_range_pan_x').attr('value', pan_x_value);
  });
  $('input.range_pan_x').dblclick(function(){
    var pan_y_value = $('input.range_pan_y').val();
    var pan_z_value = $('input.range_pan_z').val();
    dspjsTone.setPannerNode(0, pan_y_value, pan_z_value );
    $('input.range_pan_x').attr('value', 0);
    $('input.text_range_pan_x').attr('value', 0);
  });
  $('input.range_pan_y').change(function(){
    var pan_x_value = $('input.range_pan_x').val();
    var pan_y_value = $('input.range_pan_y').val();
    var pan_z_value = $('input.range_pan_z').val();
    dspjsTone.setPannerNode(pan_x_value, pan_y_value, pan_z_value );
    $('input.text_range_pan_y').attr('value', pan_y_value);
  });
  $('input.range_pan_y').dblclick(function(){
    var pan_x_value = $('input.range_pan_x').val();
    var pan_z_value = $('input.range_pan_z').val();
    dspjsTone.setPannerNode(pan_x_value, 0, pan_z_value );
    $('input.range_pan_y').attr('value', 0);
    $('input.text_range_pan_y').attr('value', 0);
  });
  $('input.range_pan_z').change(function(){
    var pan_x_value = $('input.range_pan_x').val();
    var pan_y_value = $('input.range_pan_y').val();
    var pan_z_value = $('input.range_pan_z').val();
    dspjsTone.setPannerNode(pan_x_value, pan_y_value, pan_z_value );
    $('input.text_range_pan_z').attr('value', pan_z_value);
  });
  $('input.range_pan_z').dblclick(function(){
    var pan_x_value = $('input.range_pan_x').val();
    var pan_y_value = $('input.range_pan_y').val();
    dspjsTone.setPannerNode(pan_x_value, pan_y_value, 0 );
    $('input.range_pan_z').attr('value', 0);
    $('input.text_range_pan_z').attr('value', 0);
  });

  $('#SamplingRate').change(function(){
    dspjsTone.setSampleRate($('input[name=samplingRate]:checked').val());
  });

  $('input.note_A').click(function(){
    var button_status=$("input.note_A").val();
    switch ( button_status ) {
      case 'Hit_A':
      dspjsTone.noteOn(69);
      $("input.note_A").attr("value", "Stop_A");
      break;
     case 'Stop_A':
      dspjsTone.noteOff(69);
      $("input.note_A").attr("value", "Hit_A");
      break;
    }
  });

  $('input.wsConnect').click(function(){
    var status = $('input.wsConnect').val();
    switch(status) {
      case 'Connect':
      var wsIp = $('input.wsIp').val();
      var wsPort = $('input.wsPort').val();
      createWebSocket( wsIp, wsPort );
      $('img.lightbulb').attr('src', 'images/Light_Bulb_On.png');
      $('input.wsConnect').attr('value', 'Disconnect');
      break;
     case 'Disconnect':
      $('img.lightbulb').attr('src', 'images/Light_Bulb_Off.png');
      $('input.wsConnect').attr('value', 'Connect');
      closeWebSocket( );
      break;
    }
  });
  $('div.wsConnect').click(function(){
    var status = $('input.hiddenVal').val();
    switch(status) {
      case 'Connect':
      var wsIp = $('input.wsIp').val();
      var wsPort = $('input.wsPort').val();
      $('img.lightbulb').attr('src', 'images/Light_Bulb_On.png');
      $('input.wsConnect').attr('value', 'Disconnect');
      $('input.hiddenVal').attr('value', 'Disconnect');
      createWebSocket( wsIp, wsPort );
      break;
     case 'Disconnect':
      $('img.lightbulb').attr('src', 'images/Light_Bulb_Off.png');
      $('div.wsConnect').attr('value', 'Connect');
      $('input.hiddenVal').attr('value', 'Connect');
      closeWebSocket( );
      break;
    }
  });

  
});

function wsOpen(){
  $('img.lightbulb').attr('src', 'images/Light_Bulb_On.png');
  $('input.wsConnect').attr('value', 'Disconnect');
}

function wsClose(){
  $('img.lightbulb').attr('src', 'images/Light_Bulb_Err.png');
}

function wsError(){
  wsClose();
}
