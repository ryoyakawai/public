window.addEventListener("message", webMidiLinkRecv, false);
function webMidiLinkRecv(event) {
  var msg = event.data.split(",");
  console.log(msg);
  switch (msg[0]) {
   case "midi":
    switch (parseInt(msg[1], 16) & 0xf0) {
     case 0x80:
      //app.NoteOff(parseInt(msg[2], 16));
      dspjsTone.noteOff(parseInt(msg[2], 16));
      break;
     case 0x90:
      if (msg[3] > 0) {
        //app.NoteOn(parseInt(msg[2], 16), parseInt(msg[3], 16));
        dspjsTone.noteOn(parseInt(msg[2], 16), parseInt(msg[3], 16));
      } else {
        //app.NoteOff(parseInt(msg[2], 16));
        dspjsTone.noteOff(parseInt(msg[2], 16));
      }
      break;
     case 0xb0:
      if (parseInt(msg[2], 16) == 0x78) {
        console.log('stop', dspjsTone);
        //app.AllOff();
        dspjsTone.allNoteOff();
      }
      break;
    }
    break;
  }
}
   