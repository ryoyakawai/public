$(function(){
});

function BufferLoader(context) {
  this.context = context;
  this.loadCount = 0;
  this.soundBuffer = new Array();
};

var sourceCount = 0;
BufferLoader.prototype.loadBuffer = function (url, key) {
  xhrjsTone.soundBuffer[key] = this.context.createBufferSource();
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.onreadystatechange = checkReadyState;
  xhr.responseType = 'arraybuffer';
  xhr.onload = function() {
    // 実在するオブジェクトでないとcreateBufferできない
    // this.hogeとかはNG
    xhrjsTone.soundBuffer[key] = xhrjsTone.context.createBuffer(xhr.response, false);
  };
  sourceCount++;
  xhr.send();
  
  xhr.onerror = function() {
    console.log('BufferLoader XHR ERROR');
  };
  
  // call when termination of the xhr request 
  function checkReadyState(){
    if (xhr.readyState == 4 && xhr.status == 200){
      //console.log('READY:', url);
      sourceCount--;
      if(sourceCount==0) {
        completeLoading();
      }
    }
  }
  
  // call when error occuered in xhr request 
  function onError(){
    console.log('Load Error:', url);
  }
  
};


function convertVelocity(midiVelocity)
{
  return midiVelocity / 127;
}

var xhrjsTone = function() {

  return {
    init: function(context, canvas_context){ 
      this.context = context;
      this.pannerNode = this.context.createPanner();
      this.pannerPosition = { "x": 0, "y": 0, "z": 0 };this.presetStartDelay = 0;
      this.presetStartDelay = 0;
      this.src = new Array();
      this.osc = new Array();
      this.noteTone = new Array();
      this.bufsize = new Array();
      this.sampleRate = 44100;
      this.amplitude = '0';
      this.canvas_context = canvas_context;
      this.lastkeyCode = null;
      this.soundBuffer = new Array();
      var baseDir = soundSource.baseDir;
      
      var bufferLoader = new BufferLoader(this.context);
      for( key in soundSource.sources ) {
        var url = baseDir + soundSource.sources[key].path;
        bufferLoader.loadBuffer(url, key);
      }
    },

    setOscType: function(type) {
      this.oscType = type;
    },

    setAmplitude: function(num) {
      this.amplitude = num;
    },

    setSampleRate: function(num) {
      this.sampleRate = num;
    },

    setPannerNode: function(x, y, z) {
      if(typeof(x) == "") x = 0;
      if(typeof(y) == "") y = 0;
      if(typeof(z) == "") z = 0;
      
      this.pannerPosition = { "x": x, "y": y, "z": z };
      this.pannerNode.setPosition(x, y, z);
    },
    
    noteOn: function(note, midiVelocity) {
      if(typeof midiVelocity == 'undefined') midiVelocity = 80;
      var webAudioVelocity = convertVelocity(midiVelocity);
      
      if(this.soundBuffer[note] == undefined) {
        note=81;
      }

      this.src[note] = this.context.createBufferSource();
      this.src[note].buffer = this.soundBuffer[note];
      this.src[note].gainNode = this.context.createGainNode();
      this.src[note].gainNode.gain.value = 4 * webAudioVelocity - 1;
      this.src[note].connect(this.src[note].gainNode);

      this.pannerNode.setPosition(this.pannerPosition.x, this.pannerPosition.y, this.pannerPosition.z);
      this.src[note].gainNode.connect(this.pannerNode);
      this.pannerNode.connect(this.context.destination);

      this.src[note].loop = false;
      this.src[note].noteOn(0);

      // canvas
      this.src[note].connect(canvas07.analyser);
      animationLoopTime();
      canvas07.analyser.connect(this.context.destination);
      // canvas

    },

    noteOff: function(note) {
      /*
      note=81;
      if(note=='') note=0;
      this.src[note].noteOff(0);
      */
    },

    allNoteOff: function(note) {
      for(var i=0; i<127; i++) {
        if(typeof this.src[i] != 'undefined') {
          this.noteOff(i);
        }
      }
    }
  }
}();

function isObject(obj) {
  return obj instanceof Object && Object.getPrototypeOf(obj) === Object.prototype;
}

// canvas
function render(ele, ctx, data){
  var ctx01 = ctx;
  
  //背景
  ctx01.beginPath();
  ctx01.fillStyle = 'black';
  ctx01.rect(0, 0, ele.width, ele.height);
  ctx01.closePath();
  ctx01.fill();
  //基準線
  ctx01.beginPath();
  ctx01.strokeStyle = 'red';
  ctx01.moveTo(0, ele.height/2);
  ctx01.lineTo(ele.width, ele.height/2);
  ctx01.closePath();
  ctx01.stroke();
  //波形
  var value;
  ctx01.beginPath();
      ctx01.strokeStyle = 'gray';
  ctx01.moveTo(0,-999);
  for (var i = 0; i < data.length; ++i){
				    value = (parseInt(data[i]) - 128) / 2 + ele.height / 2;
				ctx01.lineTo(i,value);
  }
  ctx01.moveTo(0,999);
  ctx01.closePath();
  ctx01.stroke();
}

var cnt=0;
function animationLoopTime(){
  cnt++;
  if(cnt%2){
    canvas07.analyser.getByteTimeDomainData(canvas07.timeDomainData);
    //analyser.getByteFrequencyData(timeDomainData);
    render(canvas01, canvas07.canvas_context, canvas07.timeDomainData);
  }
  requestAnimationFrame(animationLoopTime);
}



//set requestAnimationFrame to window (with vendor prefixes)
(function (w, r){
  w['r'+r] = w['r'+r] || w['webkitR'+r] || w['mozR'+r] || w['msR'+r] || w['oR'+r] || function(c){ w.setTimeout(c, 1000 / 60); };
})(window, 'equestAnimationFrame');

