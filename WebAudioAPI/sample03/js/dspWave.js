var DspWave = function( sampleRate, bufferSize, noteFreq, note, waveType, amplitude ) {
  this.sampleRate = sampleRate;
  this.bufSize = bufferSize;
  this.noteFreq = noteFreq;
  this.note = note;
  this.yStep = 0;
  this.phase = 0;
  this.waveType = waveType;
  this.amplitude = amplitude;
};

DspWave.prototype.stop =function() {
  this.eg.note_off();
  this.feg.note_off();
};

DspWave.prototype.setEnvelope = function(a ,d, s, r) {
  this.eg.set_a(a);
  this.eg.set_d(d);
  this.eg.set_s(s);
  this.eg.set_r(r);

  this.feg.set_a(a);
  this.feg.set_d(d);
  this.feg.set_s(s);
  this.feg.set_r(r);
};

DspWave.prototype.play =function() {
  // START: preparing DSP
  this.vco1 = new VCO(this.sampleRate, this.noteFreq, this.waveType);
  this.vco2 = new VCO(this.sampleRate, this.noteFreq, this.waveType);
  
  this.vco1.amplitude = this.amplitude;
  this.vco2.amplitude = this.amplitude;
  
  this.eg = new EG();
  this.feg = new EG();
  
  this.eg.note_on();
  this.feg.note_on();
  
  var f1 = this.noteFreq; //Math.pow(2.0, (this.vco1.oct + this.note + 3 - 3 * 12 + this.vco1.fine) / 12.0);
  var f2 = f1;
  
  console.log('vco1', this.vco1, 'noteFreq', this.noteFreq);
  // END: preparing DSP
  
  var self = this;
  dspjsTone.osc[this.note].node.onaudioprocess = function(event) {
    self.setEnvelope(dspjsTone.a, dspjsTone.d, dspjsTone.s, dspjsTone.r);

    var Lch = event.outputBuffer.getChannelData(0);
    var Rch = event.outputBuffer.getChannelData(1);
    var s1 = self.vco1.next(f1);
    var s2 = self.vco1.next(f2);
 
    for(var i=0; i<=Lch.length; i++ ) {    
      Lch[i] =( s1[i] + s2[i] ) * self.eg.gain;
      Rch[i] = Lch[i];
      self.eg.next();
      self.feg.next();
    }
  };
  
  
};

// VCO // // // // // // // // // // // //
var WAVE = {
  TRI: 0,
  SAW: 1,
  SQUARE: 2,
  SINE: 3
};

var VCO = function(sampleRate, noteFreq, wave_type) {
  //this.frequency = 110;
  this.phase = 0.0;
  this.sampleRate = sampleRate;
  this.phaseStep = 1 / this.sampleRate;

  this.oct   = 0;
  this.fine  = 0;
  this.wave  = wave_type;
  this.gain  = 0.5;
  this.on    = 1;
};

VCO.prototype.SineNext = function(p) {
  var phase = this.phase;
  var PI_2 = 2 * Math.PI ; 
  var ret = Math.sin( PI_2 * phase * p);
  this.phase = phase + this.phaseStep;
  return ret;
};

VCO.prototype.SquareNext = function(p) {
  var phase = this.phase;
  var w = 2 / p;
  if (phase > w)// if (this.phase * p * Math.PI > 2 * Math.PI)
    phase -= w;
  var ret = phase * p > 1 ? 0.8 : -0.8;
  this.phase = phase + this.phaseStep;
  return ret;
};

VCO.prototype.SawNext = function(p) {
  var phase = this.phase;
  var w = 2 / p;
  if (phase > w)
    phase -= w;
  var r = phase * p;
  var ret = r - 1;
  this.phase = phase + this.phaseStep;
  return ret;
};

VCO.prototype.TriNext = function(p) {
  var phase = this.phase;
  var w = 2 / p;
  if (phase > w)
    phase -= w;
  var r = phase * p;
  var ret = 2 * ((r >= 1 ? 2 - r : r) - 0.5);
  this.phase = phase + this.phaseStep;
  return ret;
};

VCO.prototype.next = function(p) {
  var stream = [];
  var i, imax;
  var stream_length = 4096;
  if (this.on == 1) {    
    switch (this.wave) {
     case WAVE.TRI:
      for (i = 0, imax = stream_length; i < imax; i++)
        stream[i] = this.TriNext(p) * this.gain;
      break;
     case WAVE.SAW:
      for (i = 0, imax = stream_length; i < imax; i++)
        stream[i] = this.SawNext(p) * this.gain;
      break;
     case WAVE.SQUARE:
      for (i = 0, imax = stream_length; i < imax; i++)
        stream[i] = this.SquareNext(p) * this.gain;
      break;
     case WAVE.SINE:
      for (i = 0, imax = stream_length; i < imax; i++)
        stream[i] = this.SineNext(p) * this.gain;
      break;
    }
  } else {
    for (i = 0, imax = stream_length; i < imax; i++) {
      stream[i] = 0;
    }
  }
  return stream;
};

VCO.prototype.set_pitch = function(p) {
  this.oct = Math.floor((p + 25) / 50) * 12;
};

VCO.prototype.set_fine = function(p) {
  this.fine = (p - 50) / 100;
};

VCO.prototype.set_wave = function(val) {

  this.wave = Math.floor((val + 25) / 50);
  };

VCO.prototype.set_gain = function(val) {
  this.gain = val / 100;
};

VCO.prototype.set_on = function(val) {
  this.on = val;
};

VCO.prototype.set_glide_time = function(val) {
  this.glide.set_time(val);
};

VCO.prototype.set_glide_on = function(val) {
  this.glide.on = val;
};

VCO.prototype.set_goal_pitch = function(val) {
  this.glide.goal_pitch = val;
};

// EGM // // // // // // // // // // // //
var EGM = {
  Idle    : 0,
  Attack  : 1,
  Decay   : 2,
  Sustain : 3,
  Release : 4
};

var EG = function() {
  this.gain = 0.0;
  this.time = 0;
  this.mode = EGM.Idle;
  this.a = 0;
  this.d = 20;
  this.s = 100 / 100;
  this.r = 20;
  
  this.a_max = 100;
  this.d_max = 100;
  this.r_max = 100;
  
  this.a_delta = 1.0 / (this.a * 500 + 1);
  this.d_delta = 1.0 / ((this.d + 5) * 1000 + 1);
  this.r_delta = 1.0 / (this.r * 1000 + 1);
};

EG.prototype.set_a = function(val) {
  this.a = val;
  this.a_delta = 1.0 / (this.a * 500 + 1);
};
EG.prototype.set_d = function(val) {
  this.d = val;
  this.d_delta = 1.0 / ((this.d + 5) * 1000 + 1);
};
EG.prototype.set_s = function(val) {
  this.s = val / 100.0;
};

EG.prototype.set_r = function(val) {
  this.r = val;
  this.r_delta = 1.0 / (this.r * 1000 + 1);;
};

EG.prototype.note_on = function() {
  this.gain = 0.0;
  this.time = 0;
  this.mode = EGM.Attack;
};

EG.prototype.note_off = function() {
  this.mode = EGM.Release;
};

EG.prototype.next = function() {
  this.time = 0;
  switch (this.mode) {
   case EGM.Attack:
    this.gain += this.a_delta;
    if (this.gain >= 1.0) {
      this.gain = 1.0;
      this.mode = EGM.Decay;
    }
    break;
   case EGM.Decay:
    this.gain -= this.d_delta;
    if (this.gain <= this.s) {
      this.gain = this.s;
      this.mode = EGM.Sustain;
    }
    break;
   case EGM.Sustain:
    break;
   case EGM.Release:
    this.gain -= this.r_delta;
    if (this.gain <= 0.0) {
      this.gain = 0.0;
      this.mode = EGM.Idle;
    }
    break;
  }
};
