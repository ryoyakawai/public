var DspjsTone = function() {
};

DspjsTone.prototype.init = function(context, canvas_context) {
  // creating DoReMi Freq
  var r = Math.pow( 2.0, 1.0 / 12.0 );
  this.noteDefMIDI = new Array();
  this.noteDefMIDI[69] = 440 ;
  for ( var i = 70; i <= 127; i++ ) {
    this.noteDefMIDI[i] = this.noteDefMIDI[i-1] * r;
  }
  for (  i = 68; i >= 0; i-- ) {
    this.noteDefMIDI[i] = this.noteDefMIDI[i+1] / r;
  }
  this.context = context;
  this.pannerNode = this.context.createPanner();
  this.pannerPosition = { "x": 0, "y": 0, "z": 0 };
  this.presetStartDelay = 0;
  this.src = new Array();
  this.oscBufferIsSet = new Array();
  this.osc = new Array();
  this.noteTone = new Array();
  this.bufsize = new Array();
  this.sampleRate = 44100;
  this.amplitude = '0';
  this.wave_type = 'DSP.SINE';
  this.oscType='sine';
  this.canvas_context = canvas_context;
  this.lastkeyCode=null;
  this.noteOnFlag = new Array();
  for(var i=0; i<=127; i++) {
    this.oscBufferIsSet[i] = {
      dry: {
        'DSP.TRIANGLE': false,
        'DSP.SAW'     : false,
        'DSP.SQUARE'  : false,
        'DSP.SINE'    : false
      }
    };
    this.osc[i] = {
      'dry': {
        'DSP.TRIANGLE': false,
        'DSP.SAW'     : false,
        'DSP.SQUARE'  : false,
        'DSP.SINE'    : false
      },
      'wet': {
        'DSP.TRIANGLE': false,
        'DSP.SAW'     : false,
        'DSP.SQUARE'  : false,
        'DSP.SINE'    : false
      }
    };
    this.noteOnFlag[i]=false;
  }
  this.envelope = {
    a: 0.01,
    d: 4,
    s: 8,
    r: 4
  };  
};


DspjsTone.prototype.setOscType = function(type) {
  this.oscType = type;
};

DspjsTone.prototype.setAmplitude = function(num) {
  this.amplitude = num; 
};

DspjsTone.prototype.setSampleRate = function(num) {
  this.sampleRate = num;
};

DspjsTone.prototype.setPannerNode = function(x, y, z) {
  if(typeof(x) == "") x = 0;
  if(typeof(y) == "") y = 0;
  if(typeof(z) == "") z = 0;
  
  this.pannerPosition = { "x": x, "y": y, "z": z };
  this.pannerNode.setPosition(x, y, z);
};
    
DspjsTone.prototype.setEnvelope = function(a, d, s, r) {
  this.a = a;
  this.d = d;
  this.s = s;
  this.r = r;
};

DspjsTone.prototype.jsNoteOn = function( note_in ) {
  var note = note_in;
  if(this.noteOnFlag[note]==false) {
    var note_freq = this.noteDefMIDI[note];
    var midiVelocity = 120;
    var webAudioVelocity = convertVelocity(midiVelocity);
    this.noteOnFlag[note]=true;
    
    oscType=this.oscType;
    switch(oscType) {
     case 'triangle':
      this.wave_type=WAVE.TRI;
      break;
     case 'saw':
    default:
      this.wave_type=WAVE.SAW;
      break;
     case 'square':
      this.wave_type=WAVE.SQUARE;
      break;
     case 'sine':
      this.wave_type=WAVE.SINE;
      break;
    }
    this.bufsize[note] = 4096; // 92msec
    
    // STAT: javascriptNode
    this.osc[note].node = this.context.createJavaScriptNode(this.bufsize[note], 1, 2);
        
    this.osc[note].dspWave = new DspWave(this.context.sampleRate, this.bufsize[note], note_freq, note, this.wave_type, this.amplitude);
    this.osc[note].dspWave.play();
    // END: javascriptNode
    
    // canvas
    this.osc[note].node.connect(canvas07.analyser);
    animationLoopTime();
    canvas07.analyser.connect(this.context.destination);
    // canvas
  }
};

DspjsTone.prototype.jsNoteOff = function(note_in) {
  var note = note_in;
  if(note=='') note=0;
  this.noteOnFlag[note]=false;
  this.osc[note].dspWave.stop();
};

var dspjsTone = new DspjsTone();


// ===============================================


function isObject(obj) {
  return obj instanceof Object && Object.getPrototypeOf(obj) === Object.prototype;
}

// canvas
function render(ele, ctx, data){
  var ctx01 = ctx;
  
  //背景
  ctx01.beginPath();
  ctx01.fillStyle = 'black';
  ctx01.rect(0, 0, ele.width, ele.height);
  ctx01.closePath();
  ctx01.fill();
  //基準線
  ctx01.beginPath();
  ctx01.strokeStyle = 'red';
  ctx01.moveTo(0, ele.height/2);
  ctx01.lineTo(ele.width, ele.height/2);
  ctx01.closePath();
  ctx01.stroke();
  //波形
  var value;
  ctx01.beginPath();
      ctx01.strokeStyle = 'gray';
  ctx01.moveTo(0,-999);
  for (var i = 0; i < data.length; ++i){
				    value = (parseInt(data[i]) - 128) / 2 + ele.height / 2;
				ctx01.lineTo(i,value);
  }
  ctx01.moveTo(0,999);
  ctx01.closePath();
  ctx01.stroke();
}

var cnt=0;
function animationLoopTime(){
  cnt++;
  if(cnt%2){
    canvas07.analyser.getByteTimeDomainData(canvas07.timeDomainData);
    //analyser.getByteFrequencyData(timeDomainData);
    render(canvas01, canvas07.canvas_context, canvas07.timeDomainData);
  }
  requestAnimationFrame(animationLoopTime);
}



//set requestAnimationFrame to window (with vendor prefixes)
(function (w, r){
  w['r'+r] = w['r'+r] || w['webkitR'+r] || w['mozR'+r] || w['msR'+r] || w['oR'+r] || function(c){ w.setTimeout(c, 1000 / 60); };
})(window, 'equestAnimationFrame');

