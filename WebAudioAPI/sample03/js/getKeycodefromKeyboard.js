window.document.onkeyup = function(e){
  var keyCode=e.keyCode;
  dspjsTone.jsNoteOff(keyCodeMap[keyCode]);
};
window.document.onkeydown = function(e){
  var keyCode=e.keyCode;
  if( dspjsTone.noteOnFlag[keyCodeMap[keyCode]] == true ) return;
  console.log(keyCode);
  dspjsTone.jsNoteOn(keyCodeMap[keyCode]);    
};

var  keyCodeMap = {
// PCKeyCode: PianoKeyNo
  90: 69,
  83: 70,
  88: 71,
  67: 72,
  70: 73,
  86: 74,
  71: 75,
  66: 76,
  78: 77,
  74: 78,
  77: 79,
  75: 80
};
