/**
 * SendFileToClient Object v0.8
 * Copyright 2012, Ryoya KAWAI
 * Released under the BSD Licenses.
 *
 * Date: Tue May 30 12:26:37 2012 +0900
 *
 * This Object would be used when you want to get application log by file easily.
 * The log file will be 
 *
 * Useage:
 *  inclide this file in html like folow.
 *  ----- >8 ----- >8 ----- 
 *			<script type="text/javascript" src="[PATH]/sendFileToClient.js"></script>
	*			<script type="text/javascript">
	*					SendFileToClient.init('[FILE-NAME-PREFIX]', [int RecordCount]);
	*			</script>
 *  ----- >8 ----- >8 ----- 
 *  [FILE-NAME-PREFIX]: Use this value to prefix of log file name.
 *  [int RecordCount]: Specify how many records you want in a file.
 *
 * Refered URL:
 *  http://jsdo.it/ukyo/9hig
 */

var SendFileToClient = function() {

  return {
    init: function(filePrefix, dataLimit) {
      this.filePrefix = filePrefix;
      this.dataPool = new Array();
      this.dataLimit = dataLimit;
      if( isNaN(this.dataLimit) ) {
        this.dataLimit = 10;
      }
      this.dataCount = 0;
    },

    setDataWithLimit: function(data){
      this.setData(data);
      this.dataCount++;
      if( this.dataCount >= this.dataLimit ) {
        this.sendFile();
      }
    },

    setData: function(data) {
      this.dataPool.push(data);
    },
        
    resetData: function() {
      delete this.dataPool;
      this.dataPool = new Array();
      this.dataCount = 0;
    },
    
    sendFile: function() {
      var bb = new WebKitBlobBuilder();
      var datePoolString=this.dataPool.join("\n");
      this.resetData();
      bb.append(datePoolString);
      var textFile = bb.getBlob();
      var blobURL = webkitURL.createObjectURL(textFile);

      var unixTime = Math.round(new Date().getTime());
      var now = new Date();
      var nowYear = now.getFullYear();
      var nowMonth = now.getMonth() + 1;
      var nowDate = now.getDate();
      var nowHours = now.getHours();  
      var nowMinutes = now.getMinutes();
      var nowSeconds = now.getSeconds();
      var nowMilliseconds = now.getMilliseconds();
      if( (nowYear.toString()).length<4 ) nowYear = '0' + nowYear;
      if( (nowMonth.toString()).length==1 ) nowMonth = '0' + nowMonth;
      if( (nowDate.toString()).length==1 ) nowDate = '0' + nowDate;
      if( (nowHours.toString()).length==1 ) nowHours = '0' + nowHours;
      if( (nowMinutes.toString()).length==1 ) nowMinutes = '0' + nowMinutes;
      if( (nowSeconds.toString()).length==1 ) nowSeconds = '0' + nowSeconds;
      if( (nowMilliseconds.toString()).length==3 ) {
        nowMilliseconds = '0' + nowMilliseconds;
      } else if( (nowMilliseconds.toString()).length==2 ) {
        nowMilliseconds = '00' + nowMilliseconds;
      } else if( (nowMilliseconds.toString()).length==1 ) {
        nowMilliseconds = '000' + nowMilliseconds;
      } 
      var nowDateTime = nowYear + nowMonth + nowDate + "_" + nowHours + nowMinutes + nowSeconds + nowMilliseconds;

      var sendFileName = this.filePrefix + '-'+ nowDateTime + '-log.txt';
      var a = document.createElement('a');
      a.download = sendFileName;
      a.href = blobURL;
      var e = document.createEvent("MouseEvent");
      e.initEvent("click", true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
      a.dispatchEvent(e);

    }
    
  };  
}();

