$(function(){

  try {
    var context = new webkitAudioContext();
  } catch(e) {
		  alert('webkitAudioContext()をサポートしているブラウザでお試しください。(例:chrome)');
  }

  var canvas01 = document.getElementById('canvas01');
  var canvas01_context = canvas01.getContext('2d');
  dspjsTone.init(context, canvas01_context);
  canvas07.init(context, canvas01_context);
  
  // init ui
  dspjsTone.setAmplitude($('input.range_amplitude').val()/20);
  $('input.text_range_amplitude').attr('value', dspjsTone.amplitude);

  //$('input.OscillatorType').attr('input[name=oscType]', 'checked');
  $('input.OscillatorTypeShow').attr('value', $('input[name=oscType]:checked').val());
  dspjsTone.setOscType($('input[name=oscType]:checked').val());
  $("input[name='samplingRate']").val(['44100']);
  if( window.navigator.platform.indexOf('Mac')==0 ) {
    $("input[name='samplingRate']").val(['48000']);
  }  
  // init ui

  //init envelope
  var range_env_attack = $('input.range_env_attack').val();
  $('input.text_range_env_attack').attr('value', range_env_attack);
  var range_env_decay = $('input.range_env_decay').val();
  $('input.text_range_env_decay').attr('value', range_env_decay);
  var range_env_sustainLevel = $('input.range_env_sustainLevel').val();
  $('input.text_range_env_sustainLevel').attr('value', range_env_sustainLevel);
  var range_env_sustain = $('input.range_env_sustain').val();
  $('input.text_range_env_sustain').attr('value', range_env_sustain);
  var range_env_release = $('input.range_env_release').val();
  $('input.text_range_env_release').attr('value', range_env_release);
  dspjsTone.setEnvelope( range_env_attack, range_env_decay, range_env_sustain, range_env_sustain, range_env_release );
  //init envelope
  

  $('#OscillatorType').change(function(){
    dspjsTone.setOscType($('input[name=oscType]:checked').val());
    $('input.OscillatorTypeShow').attr('value', dspjsTone.oscType);
  });

  $('input.range_amplitude').change(function(){
    dspjsTone.setAmplitude($('input.range_amplitude').val()/20);
    $('input.text_range_amplitude').attr('value', dspjsTone.amplitude);
  });

  //PannerNode
  $('input.range_pan_x').change(function(){
    var pan_x_value = $('input.range_pan_x').val();
    var pan_y_value = $('input.range_pan_y').val();
    var pan_z_value = $('input.range_pan_z').val();
    dspjsTone.setPannerNode(pan_x_value, pan_y_value, pan_z_value );
    $('input.text_range_pan_x').attr('value', pan_x_value);
  });
  $('input.range_pan_y').change(function(){
    var pan_x_value = $('input.range_pan_x').val();
    var pan_y_value = $('input.range_pan_y').val();
    var pan_z_value = $('input.range_pan_z').val();
    dspjsTone.setPannerNode(pan_x_value, pan_y_value, pan_z_value );
    $('input.text_range_pan_y').attr('value', pan_y_value);
  });
  $('input.range_pan_z').change(function(){
    var pan_x_value = $('input.range_pan_x').val();
    var pan_y_value = $('input.range_pan_y').val();
    var pan_z_value = $('input.range_pan_z').val();
    dspjsTone.setPannerNode(pan_x_value, pan_y_value, pan_z_value );
    $('input.text_range_pan_z').attr('value', pan_z_value);
  });

  // Envelope: attack
  $('input.range_env_attack').change(function(){
    var range_env_attack = $('input.range_env_attack').val();
    var range_env_decay = $('input.range_env_decay').val();
    var range_env_sustainLevel = $('input.range_env_sustainLevel').val();
    var range_env_sustain = $('input.range_env_sustain').val();
    dspjsTone.setEnvelope( range_env_attack, range_env_decay, range_env_sustain, range_env_sustain, range_env_release );
    $('input.text_range_env_attack').attr('value', range_env_attack);
  });
  // Envelope: decay
  $('input.range_env_decay').change(function(){
    var range_env_attack = $('input.range_env_attack').val();
    var range_env_decay = $('input.range_env_decay').val();
    var range_env_sustainLevel = $('input.range_env_sustainLevel').val();
    var range_env_sustain = $('input.range_env_sustain').val();
    dspjsTone.setEnvelope( range_env_attack, range_env_decay, range_env_sustain, range_env_sustain, range_env_release );
    $('input.text_range_env_decay').attr('value', range_env_decay);
  });
  // Envelope: sustainLevel
  $('input.range_env_sustainLevel').change(function(){
    var range_env_attack = $('input.range_env_attack').val();
    var range_env_decay = $('input.range_env_decay').val();
    var range_env_sustainLevel = $('input.range_env_sustainLevel').val();
    var range_env_sustain = $('input.range_env_sustain').val();
    var range_env_release = $('input.range_env_release').val();
    dspjsTone.setEnvelope( range_env_attack, range_env_decay, range_env_sustain, range_env_sustain, range_env_release );
    $('input.text_range_env_sustainLevel').attr('value', range_env_sustainLevel);
  });
  // Envelope: sustain
  $('input.range_env_sustain').change(function(){
    var range_env_attack = $('input.range_env_attack').val();
    var range_env_decay = $('input.range_env_decay').val();
    var range_env_sustainLevel = $('input.range_env_sustainLevel').val();
    var range_env_sustain = $('input.range_env_sustain').val();
    var range_env_release = $('input.range_env_release').val();
    dspjsTone.setEnvelope( range_env_attack, range_env_decay, range_env_sustain, range_env_sustain, range_env_release );
    $('input.text_range_env_sustain').attr('value', range_env_sustain);
  });
  // Envelope: release
  $('input.range_env_release').change(function(){
    var range_env_attack = $('input.range_env_attack').val();
    var range_env_decay = $('input.range_env_decay').val();
    var range_env_sustainLevel = $('input.range_env_sustainLevel').val();
    var range_env_sustain = $('input.range_env_sustain').val();
    var range_env_release = $('input.range_env_release').val();
    dspjsTone.setEnvelope( range_env_attack, range_env_decay, range_env_sustain, range_env_sustain, range_env_release );
    $('input.text_range_env_release').attr('value', range_env_release);
  });

  
  $('#SamplingRate').change(function(){
    dspjsTone.setSampleRate($('input[name=samplingRate]:checked').val());
  });

  $('input.note_A').click(function(){
    var button_status=$("input.note_A").val();
    switch ( button_status ) {
      case 'Hit_A':
      dspjsTone.jsNoteOn(69);
      $("input.note_A").attr("value", "Stop_A");
      break;
     case 'Stop_A':
      dspjsTone.jsNoteOff(69);
      $("input.note_A").attr("value", "Hit_A");
      break;
    }
  });

  $('input.note_B').click(function(){
    var button_status=$("input.note_B").val();
    switch ( button_status ) {
     case 'Hit_B':
      dspjsTone.jsNoteOn(70);
      $("input.note_B").attr("value", "Stop_B");
      break;
     case 'Stop_B':
      dspjsTone.jsNoteOff(70);
      $("input.note_B").attr("value", "Hit_B");
      break;
    }
  });


});