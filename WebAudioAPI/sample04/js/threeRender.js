var renderer;
function initThree() {
  width = document.getElementById('canvas-frame').clientWidth;
  height = document.getElementById('canvas-frame').clientHeight;
  renderer = new THREE.WebGLRenderer({antialias: true});
  renderer.setSize(width, height );
  document.getElementById('canvas-frame').appendChild(renderer.domElement);
  renderer.setClearColorHex(0xFFFFFF, 1.0);
  renderer.shadowMapEnabled = true;
}

var camera;
function initCamera() {  
  camera = new THREE.PerspectiveCamera( 45 , width / height , 1 , 10000 );
  camera.position.x = 0;
  camera.position.y = 85;
  camera.position.z = -302;
  camera.up.x = 0;
  camera.up.y = 0;
  camera.up.z = 1;
}

var scene;
function initScene() {  
  scene = new THREE.Scene();
}

var light, light2;
function initLight() {  
  light = new THREE.DirectionalLight(0xFFFFFF, 1.0, 0);
  light.position.set( 0, 430, -300 );
  light.castShadow = true;
  scene.add(light);

  light2 = new THREE.AmbientLight(0x555555);
  scene.add(light2);
}

var sphere, plane;
function initObject(){
  sphere = new THREE.Mesh(
    new THREE.SphereGeometry( 10, 10, 10 ),
    new THREE.MeshLambertMaterial({color: 0xff0000, ambient:0xff0000})
  );
  scene.add(sphere);
  sphere.castShadow=true;
  sphere.position.set( 0, 0, 0 );

  plane = new THREE.Mesh(

    // Use blow two lines when you want grided floor. 
    //new THREE.PlaneGeometry( 330, 800, 10, 10 ), 
    //new THREE.MeshLambertMaterial({color: 0x999999, wireframe: true, ambient:0x999999})

    new THREE.PlaneGeometry( 230, 840, 1, 1 ), 
    new THREE.MeshLambertMaterial({color: 0x999999, ambient:0x999999})
  );
  plane.receiveShadow = true;
  plane.position.set( 0, 0, 0 );
  scene.add(plane);
}

var t = 0, t_y = 0, y_pos = 50;
var panPosition = { x:0, y:0, z:0 };
var slice = 24; // 15 degree each
function loop() {
  var status = 'start';
  renderer.clear();
  t += 0.05; // Increace number to increase speed of the object.
    
  var pos_deg = t * 360 / slice;
  var pos_rad = Math.PI * pos_deg / 180;
  
  var x = 160 * Math.cos(pos_rad);
  var y = y_pos; //50 ;
  var z = -160 * Math.sin(pos_rad);
  sphere.position.set(x, y, z);
  camera.lookAt( { x:0, y:0, z:0 } ); 
  renderer.render(scene, camera);
  window.requestAnimationFrame(loop);
  
  // Gave up to change camera position, so changing sound position. 
  // Not a good Idea...
  var pos_deg_pan = ( t + 6 ) * 360 / slice;
  var pos_rad_pan = Math.PI * pos_deg_pan / 180;
  var pan_x = 160 * Math.cos(pos_rad_pan);
  var pan_y = 50 ;
  var pan_z = -160 * Math.sin(pos_rad_pan);
      
  panPosition = { x: pan_x/160, y: pan_z/160, z: 0 };  
  xhrSound.setPanPosition( panPosition.x, panPosition.y, panPosition.z );

  if(status=='stop') {
    renderer.clear();
    delete renderer;
    window.requestAnimationFrame(stop);
    return;
  }

}

function threeStart() {
  initThree();
  initCamera();
  initScene();
  initLight();
  initObject();
  
  xhrSound.init();
  xhrSound.startPlay(0, 0, 0);
  
  loop();  
};

function threeStop() {
  xhrSound.stopPlay();
}

function stop(){
  return;
}