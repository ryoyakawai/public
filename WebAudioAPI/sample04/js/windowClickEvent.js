/*
$(document).ready(
  function(){
    $("#all").hide();
    $("#all").fadeIn("slow");
  }
);
*/

$("input.sample01_start").click(
  function() {
    var button_status=$("input.sample01_start").val();
    if(button_status=="start") {
      $("input.sample01_start").attr("value", "stop");
      $("#canvas-three").html('<div id="canvas-frame" class="sample01"></div>');
      threeStart();
    }
    if(button_status=="stop") {
      $("input.sample01_start").attr("value", "start");
      $("#canvas-frame").html('<div id="canvas-three"></div>');
      threeStop();
      window.location.reload();
    }
  }
);


window.document.onkeyup = function(e){
  var keyCode=e.keyCode;
  var button_status=$("input.sample01_start").val();
  if(button_status=="stop") {
    $("input.sample01_start").attr("value", "start");
    window.location.reload();
  }
  if(button_status=="start") {
    $("input.sample01_start").attr("value", "stop");
    $("#canvas-frame").html('<div id="canvas-frame" class="sample01"></div>');
    threeStart();
  }
};


$(function() {
    $( "#vSlider" )
      .slider({
                orientation: "vertical",
                value: 50,
                min: 0,
                max: 100,
                step: 5
              })
      .bind('slide', function(event, ui){
              var value = ui.value;
              y_pos = value;
            })
    ;
  });
