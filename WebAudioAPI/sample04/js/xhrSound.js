var sound = {};
var xhrSound = function() {
  
  return {
    init: function() {
      this.context = new webkitAudioContext();
      this.ctx = this.context;
      this.mainVolume = this.ctx.createGainNode();      
      this.soundFileList = new Array();
      soundFileList = {
        'techno'        : 'sounds/techno.mp3',
        'breakBeat'     : 'sounds/breakbeat.wav',
        'clappingCrowd' : 'sounds/clapping-crowd.wav',
        'blueyellow'    : 'sounds/blueyellow.wav',
        'brJam'         : 'sounds/br-jam-loop.wav'
      };
      //this.soundType = "clappingCrowd";
      this.soundType = "breakBeat";
    },

    setPanPosition: function(x, y, z) {
      sound.panner.setPosition( y, z, x );
      this.context.listener.setPosition( 0, 0, -0.7 );
    },

    setGainValue: function(val) {
      sound.volume.gain.value=val;
    },

    setSoundType: function(type) {
      this.soundType = type;
    },
   
    startPlay: function(x, y, z) {
      this.mainVolume.connect(this.ctx.destination);
  
      sound.source = this.ctx.createBufferSource();
      sound.panner = this.ctx.createPanner();
      sound.volume = this.ctx.createGainNode();

      sound.source.connect(sound.volume);
      sound.volume.connect(sound.panner);
      sound.panner.connect(this.ctx.destination);
      sound.source.loop = true;

      sound.volume.gain.value=0.3;

      var soundFileName = soundFileList[this.soundType];
      
      // Load a sound file using an ArrayBuffer XMLHttpRequest.
      var request = new XMLHttpRequest();
      request.open("GET", soundFileName, true);
      request.responseType = "arraybuffer";
      request.onload = function(e) {  
        // Create a buffer from the response ArrayBuffer.
        var buffer = xhrSound.ctx.createBuffer(this.response, false);
        console.log(buffer);
        sound.buffer = buffer;
        
        // Make the sound source use the buffer and start playing it.
        sound.source.buffer = sound.buffer;
        sound.source.noteOn(xhrSound.ctx.currentTime);
      };
      request.send();
    },

    stopPlay: function() {
      sound.source.disconnect();
    }

    
  };
}();


