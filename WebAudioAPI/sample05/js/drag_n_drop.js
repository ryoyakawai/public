var drop = document.getElementById("dropArea");

var sound = { };
sound.isPlaying = false;
drop.addEventListener(
  "drop", 
  function(event) {
    event.preventDefault();
    drop.style.backgroundColor = "#ccc";
    //
    if( sound.isPlaying == true) {
      alert('only one audio file playing is allowd.');
      return;
    }
    
    var filelist = event.dataTransfer.files;
    for (var i = 0; i<filelist.length; i++) {
      console.log(filelist[0]);
      if (window.FileReader && (filelist[i].type == "text/plain")) {
        var reader = new FileReader();
        reader.readAsText(filelist[i]);
        reader.onload = function(event) {
          drop.innerHTML = event.target.result;
        };
      } else if( window.FileReader && (filelist[i].type == "audio/wav" || filelist[i].type == "audio/mp3") ) {
        if(typeof(sound.context) == "undefined") sound.context = new webkitAudioContext();
        sound.source = sound.context.createBufferSource();
        sound.panner = sound.context.createPanner();
        sound.volume = sound.context.createGainNode();

        sound.source.connect(sound.volume);
        sound.volume.connect(sound.panner);
        sound.panner.connect(sound.context.destination);
        sound.source.loop = true;
        sound.volume.gain.value=0.3;

        var reader = new FileReader();
        reader.readAsArrayBuffer(filelist[i]);
        reader.onload = function(event) {
          sound.source.buffer = sound.context.createBuffer(event.target.result, false);
          sound.isPlaying = true;
          $("input.stop").show();
          $("p.stopped").hide();
          $("p.playing").show();
          sound.source.noteOn(0);
        };
      } else {
        alert('could not play that audio');
      }
    }
}, true);

function stop(){
  sound.isPlaying = false;
  sound.source.noteOff(0);
}

// ドラッグする要素をドラッグするとき
drop.addEventListener("dragenter", function(event) {
    event.preventDefault();
    drop.style.backgroundColor = "#ecc";
}, true);

// ドロップ領域を出たとき
drop.addEventListener(
  "dragleave", 
  function(event) {
    event.preventDefault();
    drop.style.backgroundColor = "#ccc";
  }, true);

// このイベントはドラッグ＆ドロップする要素には必須
drop.addEventListener(
  "dragover", 
  function(event) {
    event.preventDefault();
  },
  true);

// ドロップ領域にドロップされたとき
window.addEventListener(
  "drop", 
  function(event) { event.preventDefault();},
  true
);

// ドロップ領域に入ったとき
window.addEventListener(
  "dragenter", 
  function(event) { event.preventDefault(); }, 
  true
);

// ドロップ領域を出たとき
window.addEventListener(
  "dragleave", 
  function(event) { event.preventDefault();}, 
  true
);

// このイベントはドラッグ＆ドロップする要素には必須
window.addEventListener(
  "dragover", 
  function(event) { event.preventDefault();}, 
  true
);
