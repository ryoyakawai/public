$(function() {
  $("input.stop").hide();
  $("p.playing").hide();
}
 );

$("input.stop").click(
  function() {
    stop();
    $("input.stop").hide();
    $("p.playing").hide(); 
    $("p.stopped").show();
  }
);
