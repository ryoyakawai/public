var KnobControl = function() {
  var knobMouseDown = false;

  return {
    'setMouseDownStatus': function(status) {
      knobMouseDown = status;
    },

    'setVal': function(value) {
      knobVal.val = value;
    },

    'getVal': function() {
      return knobVal.val;
    },
    
    'setValAll': function(value) {
      knobVal = value;
    },

    'getValAll': function() {
      return knobVal;
    },

    'mouseDown':function(e, rect){
      var mx_my = this.getMxMy(e, rect);
      knobVal.start = mx_my.y;
    },
    
    'mouseMove': function(e, rect) {
      if( knobMouseDown == true ) {
        var move_mx_my = this.getMxMy(e, rect);
        if( move_mx_my.y > knobVal.start) {
          if( knobVal.val >= 0 ) {
            knobVal.val =  knobVal.val + ( knobVal.start - move_mx_my.y ) / knobVal.ticks ;
          } 
        } else {
          if (knobVal.val <= knobVal.maxOffset ) {
            knobVal.val = knobVal.val + ( -1 * ( move_mx_my.y - knobVal.start ) ) / knobVal.ticks ; 
          }
        }
        // createOffset -20 to 20
        knobVal.mixVal = 40 * knobVal.val / knobVal.maxOffset - 20;
        return knobVal;
      }
    },

    'getMxMy': function(e, rect) {
      return {'x': e.clientX - rect.left, 'y': e.clientY - rect.top};
    }
    
  };
};

var knobControl = new KnobControl();
