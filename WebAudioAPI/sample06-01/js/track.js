// create context
var context = new webkitAudioContext();

// define basicTrack
var basicTrack = function(name, number) {
  this.volume = context.createGainNode();
  this.panner = context.createPanner();
  this.panner.setPosition(0, 0, 0);
  this.mute = false;

  this.analyserNode = context.createAnalyser();
  this.analyserNode.fftSize = 2048;
  this.analyserNode.maxDecibles = 0; 
		this.drawContext = null;
		this.analyserNodeCanvas = null;
		this.canvasMeter = null;

  this.number = number;
  if(number == 0) { // master
    this.name = 'master';
    this.type = 'master';
    this.volume.gain.value = this.volume.gain.value_tmp = 0.3;
    this.volume.connect(this.panner);
    this.panner.connect(context.destination);
  } else {
    this.name = name;
    this.type = 'Track';
    this.volume.gain.value = this.volume.gain.value_tmp = 0.5;
    // Log EQ
    this.eqL = context.createBiquadFilter();
    this.eqL.type = 3;
    this.eqL.frequency.value = 440;
    this.eqL.Q.value = 0;
    this.eqL.gain.value = 0;
    this.eqLKnob = { 'val':135, 'stdVal':0, 'y':0, 'start':0, 'maxOffset':270, 'ticks': 1.3, mixVal: 0 };
    // High EQ
    this.eqH = context.createBiquadFilter();
    this.eqH.type = 4;
    this.eqH.frequency.value = 1760;
    this.eqH.Q.value = 0;
    this.eqH.gain.value = 0;
    this.eqHKnob = { 'val':135, 'stdVal':0, 'y':0, 'start':0, 'maxOffset':270, 'ticks': 1.3, mixVal: 0 };

    this.source = null; //context.createBufferSource();

    this.solo = false;
  }
};
  
// must create construct for prototype
var Track = function(){
  this.nextTrackNo = 0;
};
Track.prototype = {
  add: function(name) {
    var trackNo = this.nextTrackNo++;
    this[trackNo] = new basicTrack(name, trackNo);
    return trackNo;
  },
  delete: function(trackNo) {
    if(this[trackNo].source.playbackState==2) this[trackNo].source.disconnect();
    delete track[trackNo];
  },
  setBuffer: function(trackNo, buffer) {
    this[trackNo].preparedBuffer = context.createBuffer(buffer, false);
  },
  setGainValue: function(trackNo, value) {
    this[trackNo].volume.gain.value = this[trackNo].volume.gain.value_tmp = value;
  },
  setSourceLoop: function(trackNo, value) {
    this[trackNo].source.loop = value;
  },
  setPanner: function(trackNo, x, y, z) {
    this[trackNo].panner.setPosition(x, y, z);
  },

  getEqVal: function(trackNo, type) {
    switch(type) {
      case 'eqL':
      var val = this[trackNo].eqLKnob;
      break;
      case 'eqH':
      var val = this[trackNo].eqHKnob;
      break;
    }
    return val;
  },

  setEqVal: function(trackNo, type, value) {
    switch(type) {
     case 'eqL':
      this[trackNo].eqLKnob = value;
      this[trackNo].eqL.gain.value = value.mixVal;
      break;
      case 'eqH':
      this[trackNo].eqHKnob = value;
      this[trackNo].eqH.gain.value = value.mixVal;
      break;
    }
  },

  noteOn: function(trackNo) {
    if(trackNo == 0) {
      this[0].volume.connect(this[0].analyserNode); // connect to meter   
    } else {
      this[trackNo].source = context.createBufferSource();
      this[trackNo].source.loop=false;
      this[trackNo].source.buffer = this[trackNo].preparedBuffer;

      this[trackNo].source.connect(this[trackNo].eqL);
      this[trackNo].eqL.connect(this[trackNo].volume);
      
      this[trackNo].source.connect(this[trackNo].eqH);
      this[trackNo].eqH.connect(this[trackNo].volume);
      
      this[trackNo].volume.connect(this[trackNo].panner);
      this[trackNo].volume.connect(this[trackNo].analyserNode); // connect to meter   

      this[trackNo].panner.connect(this[0].volume);

      //this[trackNo].source.noteGrainOn(0, 15, 10);
      this[trackNo].source.noteOn(0);
    }


    //canvasMeter [begin]
    track[trackNo].drawContext = document.getElementById('canvasMeter_' + trackNo);
    track[trackNo].analyserNodeCanvas = track[trackNo].drawContext.getContext('2d');
    track[trackNo].canvasMeter = new CanvasMeter( track[trackNo].analyserNode, track[trackNo].analyserNodeCanvas );
    
	   function updateVisualiser(time) {
			   track[trackNo].canvasMeter.updateAnalyser(track[trackNo].analyserNode, track[trackNo].analyserNodeCanvas);
			   track[trackNo].canvasMeter.rafID = window.webkitRequestAnimationFrame( updateVisualiser );
	   }
    //canvasMeter [end]
    updateVisualiser(0);
    

  },
  noteOff: function(trackNo) {
    if(trackNo != 0) this[trackNo].source.disconnect();
  },
  noteAllOn: function() {
    for(var i= 0; i < this.nextTrackNo; i++ ) {
      if(typeof track[i] == 'object' ) this.noteOn(i);
    }
  },
  noteAllOff: function() {
    console.log(track);
    for(var i= 0; i < this.nextTrackNo; i++ ) {
      if(typeof track[i] == 'object' ) this.noteOff(i);
    }
  },
  mute: function(trackNo, mode) {
    if( this[trackNo].mute==false ) {
      this[trackNo].mute = true;
      this[trackNo].solo = false;      
    }
    var solo_count=0;
    for(var i=1; i < this.nextTrackNo; i++ ) {
      if(this[i].solo == true) solo_count++;
    }
    if( solo_count==0 ) {
      for(var i= 1; i < this.nextTrackNo; i++ ) {
        if(typeof track[i] == 'object' ) {
          if(this[i].mute == true ) {
            this[i].volume.gain.value=0; 
          } else {
            this[i].volume.gain.value=this[i].volume.gain.value_tmp; 
          }
        }
      }
    } else {
      this[trackNo].volume.gain.value=0; 
    }
  },
  unmute: function(trackNo, mode) {
    if( this[trackNo].mute==true ) {
      this[trackNo].mute = false;
      this[trackNo].solo = false;
    }
    var solo_count=0;
    for(var i=1; i < this.nextTrackNo; i++ ) {
      if(this[i].solo == true) solo_count++;
    }
    if( solo_count==0 ) {
      for(var i= 1; i < this.nextTrackNo; i++ ) {
        if(typeof track[i] == 'object' ) {
          if(this[i].mute == true ) {
            this[i].volume.gain.value=0; 
          } else {
            this[i].volume.gain.value=this[i].volume.gain.value_tmp; 
          }
        }
      }
    }
  },
  muteAll: function() {
    this[0].volume.gain.value=0; 
  },
  unmuteAll: function() {
    this[0].volume.gain.value=this[0].volume.gain.value_tmp; 
  },
  
  solo: function(trackNo) {
    if ( this[trackNo].solo == false ) {
      this[trackNo].mute = false;
      this[trackNo].solo = true;
    }
    for(var i= 1; i < this.nextTrackNo; i++ ) {
      if(typeof track[i] == 'object' ) {
        if(this[i].solo == false ) {
          this[i].volume.gain.value=0; 
        } else {
          this[trackNo].volume.gain.value=this[trackNo].volume.gain.value_tmp;
        }
      }
    }
  },
  unsolo: function(trackNo) {
    if( this[trackNo].solo == true ) {
      this[trackNo].solo = false;
      this[trackNo].mute = false;
    }

    for(var i= 1; i < this.nextTrackNo; i++ ) {
      if(typeof track[i] == 'object' ) {
        if(this[i].solo == false ) {
          this[i].volume.gain.value=0; 
        } else {
          this[trackNo].volume.gain.value=this[trackNo].volume.gain.value_tmp;
        }
      }
    }

    var solo_count=0;
    for(var i= 1; i < this.nextTrackNo; i++ ) {
      if( this[i].solo == true ) solo_count++;
    }

    if(solo_count==0) {
      for(var i= 1; i < this.nextTrackNo; i++ ) {
        this[i].volume.gain.value=this[i].volume.gain.value_tmp;
      }
    }
    
  }
};

// create track
var track = new Track();


// create Master
track.add('master');

// add mastertrack ui
var trackUi = TrackElement.getMasterTrack();
//windowClickEvent.addTrack('master', trackUi);
windowClickEvent.addTrack('master', trackUi);
// add mastertrack ui 
