var TrackElement = function() {
  var colorCycle = {
    '0': '#FFFFF0',
    '1': '#FFFAFA'
  };

  return {
    getMasterTrack: function() {
      var trackTemplate =
      '<div id="0" name="0" class="ui-state-default fader_master">'+
        '<span class="label label-important track_name playStatus_0">Master Track</span>'+
        '<div class="trackno"></div>'+
        '<div class="mute_master">' +
        '<span class="badge mute_all mute_0 " trackNo="0">M</span>' +
        '</div>' +
		      '<div class="fader_pan" trackNo="0"></div>'+
		      '<div class="fader_volume" trackNo="0"></div>'+
        '<div class="meterArea">' +
				    '  <canvas class="canvasMeter" id="canvasMeter_0"></canvas>' +
        '</div>' +
        '<div class="btn-group">'+
		      '<button class="btn noteAllOn" trackNo="0"><i class="icon-play"></i></button>'+
        '<button class="btn noteAllOff" trackNO="0"><i class="icon-stop"></i></button>'+
        '</div>'+
 	      '<div class="label label-success track_name" trackNo="0">MasterTrack</div>'+
        '</div>'+
        '';
      return trackTemplate;
    },
    
    getTrackTemplate: function(trackNo, fileInfo) {
      var trackName= fileInfo.name.replace('.mp3', ' ').replace('.wav', ' ').replace('.ogg', ' ').replace('1901_', ' ').replace('_', ' ');
      var trackColor = colorCycle[trackNo%2];
      var trackTemplate =
        '<div id="'+ trackNo +'" class="ui-state-default fader_tracks" name="'+ trackNo +'">'+
//        '<span class="sortableDrag label-default">aa</span>' +
        '<div class="topLevel">' +
        ' <span class="playStatus_' + trackNo + ' badge sortableDrag">'+ trackNo + '</span>'+
        ' <button type="submit" class="btn btn-mini deleteTrack pull-right " trackNo="'+ trackNo +'" name="deleteTrack" value="Delete Track" align=><i class="icon-remove" trackNo="'+ trackNo +'"></i></button>' + 
        '</div>' +
        '<div class="mute_solo">' +
        ' <span class="badge mute mute_' + trackNo + '" trackNo="'+ trackNo +'">M</span>' +
        ' <span class="badge solo solo_' + trackNo + '" trackNo="'+ trackNo +'">S</span>' +
        '</div>' +
        '<br class="clearleft">' +
        '<div class="knobArea">' +
        ' <div class="knob knobEqL" trackNo="'+ trackNo +'"></div>' +
        ' <div class="knob knobEqH" trackNo="'+ trackNo +'"></div>' +
        '</div>' +
        '<br class="clearleft">' +
        '<div class="trackno"></div>'+
		      '<div class="fader_pan" trackNo="'+ trackNo +'"></div>'+
		      '<div class="fader_volume" trackNo="'+ trackNo +'"></div>'+
        '<div class="meterArea">' +
				    '  <canvas class="canvasMeter" id="canvasMeter_'+ trackNo +'"></canvas>' +
        '</div>' +
	       '<div class="btn-group pull-center">' +
        '  <button type="submit" class="noteOn btn" trackNo="'+ trackNo +'" name="noteOn" value="noteOn"><i class="icon-play"></i></button>' + 
        '  <button type="submit" class="noteOff btn" trackNo="'+ trackNo +'" name="noteOff" value="noteOff"><i class="icon-stop" trackNo="'+ trackNo +'"></i></button>' + 
        '</div>'+
 	      '<div class="label label-success track_name sortableDrag" trackNo="'+ trackNo +'">' + trackName + '</div>'+
        '</div>'+
        ''+
		      //'<BR><BR>'+
        '';
      return trackTemplate;
    }
  };

}();
  
