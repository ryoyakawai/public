$(function() {
  // sortable
		$( "#sortable" ).sortable({
    handle: 'div.sortableDrag' 
  });
		$( "#sortable" ).disableSelection();
});

$(function(){
  $("button.noteAllOn").live('click', (function(event) {
    var activeTrackCount = 0;
    for(var i= 1; i < track.nextTrackNo; i++ ) {
      if(typeof track[i] == 'object' ) activeTrackCount++;
    }
    if( track.nextTrackNo > 1 && activeTrackCount > 0) {
      if(event.target.tagName == 'INPUT') {
        var trackNo = $(this).attr('trackNo');
      }
      for(var i=1; i < track.nextTrackNo; i++) {
        $("span.playStatus_" + i).addClass("label-warning");
      }
      track.noteAllOn(0, 0);
    } else {
      alert('Drop audio file in the gray filed.');
    }
  }));
  $("button.noteAllOff").live('click', (function(event) {
        var activeTrackCount = 0;
    for(var i= 1; i < track.nextTrackNo; i++ ) {
      if(typeof track[i] == 'object' ) activeTrackCount++;
    }
    if( track.nextTrackNo > 1 && activeTrackCount > 0 ) {
      if(event.target.tagName == 'INPUT') {
        var trackNo = $(this).attr('trackNo');
      }
      for(var i=0; i < track.nextTrackNo; i++) {
        $("span.playStatus_" + i).removeClass("label-warning");
      }
      track.noteAllOff(0, 0);
    } else {
          alert('Drop audio file in the gray filed.');
    }
  }));

 $('.tracks').delegate('.mute, .solo', 'click', function(event){
   var type = $(this).hasClass('mute') ? 'mute' : 'solo',
       active = $(this).hasClass('badge-warning'),
       trackNo = $(this).attr('trackNo');
   switch (type){
    case 'mute':
     $(this).toggleClass('badge-warning');
     $(this).siblings().removeClass('badge-warning');
     if( active ) {
       track.unmute(trackNo, '.mute');
     } else {
       track.mute(trackNo, '.mute');
     }
     break;
    case 'solo':
     $(this).toggleClass('badge-warning');
     $(this).siblings().removeClass('badge-warning');
     if( active ) {
       track.unsolo(trackNo);
     } else {
       track.solo(trackNo);
     }
     break;
   }
  });

 $('.master').delegate('.mute_all', 'click', function(event){
   var active = $(this).hasClass('badge-warning'),
       trackNo = 0;
     $(this).toggleClass('badge-warning');
     $(this).siblings().removeClass('badge-warning');
     if( active ) {
       track.unmuteAll();
     } else {
       track.muteAll();
     }
  });

});
  
var WindowClickEvent = function() {
  return {
    addTrack : function(section, contents){
      $('div.' + section)
        .append( contents )
        .find('.fader_volume')
        .bind('slide', function(event, ui){
          var trackNo = $(this).attr('trackNo');
          var value = ui.value;
          track.setGainValue(trackNo, value);
        })
        .slider({
          orientation: "vertical",
          value: 0.8,
          min: 0,
          max: 1,
          step: 0.1
        })
        .end()
        .find('.fader_pan')
        .slider({
          value: 0,
          min: -1,
          max: 1,
          step: 0.1
        })
        .bind('slide', function(event, ui){
          var trackNo = $(this).attr('trackNo');
          var value = ui.value;
          track.setPanner(trackNo, value, 0, 0);
        })
        .bind('dblclick', function(event){
          var trackNo = $(this).attr('trackNo');
          $(this).slider('option', 'value', 0);
          track.setPanner(trackNo, 0, 0,  0);
        })
        .end()
        .find('.noteOn')
        .bind('click', function(event){
          var trackNo = $(this).attr('trackNo');
          $("span.playStatus_" + trackNo).addClass("label-warning");
          track.noteOn(trackNo, 0);
        })
        .end()
        .find('.noteOff')
        .bind('click', function(event){
          var trackNo = $(this).attr('trackNo');
          $("span.playStatus_" + trackNo).removeClass("label-warning");
          track.noteOff(trackNo, 0);
        })
        .end()
        .find('.deleteTrack')
        .bind('click', function(event){
          var trackNo = $(this).attr('trackNo');
          track.delete(trackNo);
          $("div#" + trackNo).remove();
         })
        .end()
        .find(".knobEqL")
        .bind('mousedown', function(event) {
          var trackNo = $(this).attr('trackNo');
          knobControl.setValAll(track.getEqVal(trackNo, 'eqL'));
          var rect = event.target.getBoundingClientRect();
          knobControl.setMouseDownStatus( true );
          knobControl.mouseDown(event, rect);
        })
        .bind('mousemove', function(event){
          var trackNo = $(this).attr('trackNo');
          var rect = event.target.getBoundingClientRect();
          var knobVal = knobControl.mouseMove(event, rect);
          if(typeof knobVal != 'undefined') {
            $(this).css("-webkit-transform", "rotate(" + knobVal.val +"deg)" );
            track.setEqVal(trackNo, 'eqL', knobVal);
          }
        }).bind('mouseout', function(){
          knobControl.setMouseDownStatus( false );
        }).bind('mouseup', function(){
          knobControl.setMouseDownStatus( false );
        })
        .end()
        .find(".knobEqH")
        .bind('mousedown', function(event) {
          var trackNo = $(this).attr('trackNo');
          knobControl.setValAll(track.getEqVal(trackNo, 'eqH'));
          var rect = event.target.getBoundingClientRect();
          knobControl.setMouseDownStatus( true );
          knobControl.mouseDown(event, rect);
        })
        .bind('mousemove', function(event){
          var trackNo = $(this).attr('trackNo');
          var rect = event.target.getBoundingClientRect();
          var knobVal = knobControl.mouseMove(event, rect);
          if(typeof knobVal != 'undefined') {
            $(this).css("-webkit-transform", "rotate(" + knobVal.val +"deg)" );
            track.setEqVal(trackNo, 'eqH', knobVal);
          }
        }).bind('mouseout', function(){
          knobControl.setMouseDownStatus( false );
        }).bind('mouseup', function(){
          knobControl.setMouseDownStatus( false );
        })
        .end()
        ;
    }
  };
};

var windowClickEvent = new WindowClickEvent();


