window.document.onkeydown = function(e){
  var keyCode=e.keyCode;
  switch( keyCode ) {
   case 119:
    if( track[0].nowPlaying == false ) {
      track.noteAllOn();
    } else {
      track.noteAllOff();
    }
    break;
  }
  
};

$(function() {
  // sortable
		$( "#sortable" ).sortable({
    handle: 'div.sortableDrag' 
  });
		$( "#sortable" ).disableSelection();

  // sortable trackEvent
		$( "#sortableEvent" ).sortable({
    handle: 'div.sortableEventDrag' 
  });
		$( "#sortableEvent" ).disableSelection();
});



$(function(){
  $("div.noteAllOn")
    .live('click', (function(event) {
      var activeTrackCount = 0;
      for(var i= 1; i < track.nextTrackNo; i++ ) {
        if(typeof track[i] == 'object' ) activeTrackCount++;
      }
      if( track.nextTrackNo > 1 && activeTrackCount > 0) {
        if(event.target.tagName == 'INPUT') {
          var trackNo = $(this).attr('trackNo');
        }
        for(var i=1; i < track.nextTrackNo; i++) {
          $("span.playStatus_" + i).addClass("label-warning");
        }
        track.noteAllOn(0, 0);
      } else {
        alert('Drop audio file in the gray filed.');
      }
    }))
    .live('mousedown', function(event) {
      $(this).css('border', 'solid 1px #000000');
      $('div.playButton').css('border-color', '#1e90ff transparent transparent');
      console.log('mouseDown');
    })
    .live('mouseup', function(event) {
      $(this).css('border', 'solid 1px #a9a9a9');
      //$('div.playButton').css('border-color', '#000000 transparent transparent');
    })
    .live('mouseout', function(event) {
      $(this).css('border', 'solid 1px #a9a9a9');
      //$('div.playButton').css('border-color', '#000000 transparent transparent');
    });

  $("div.noteAllOff").live('click', (function(event) {
    var activeTrackCount = 0;
    for(var i= 1; i < track.nextTrackNo; i++ ) {
      if(typeof track[i] == 'object' ) activeTrackCount++;
    }
    if( track.nextTrackNo > 1 && activeTrackCount > 0 ) {
      if(event.target.tagName == 'INPUT') {
        var trackNo = $(this).attr('trackNo');
      }
      for(var i=0; i < track.nextTrackNo; i++) {
        $("span.playStatus_" + i).removeClass("label-warning");
      }
      track.noteAllOff(0, 0);
    } else {
          alert('Drop audio file in the gray filed.');
    }
  }))
    .live('mousedown', function(event) {
      $(this).css('border', 'solid 1px #000000');
      $('div.stopButton').css('background-color', '#ffffff');
      $('div.stopButton').css('border', 'solid 1px #ffffff');
      console.log('mouseDown');
    })
    .live('mouseup', function(event) {
      $(this).css('border', 'solid 1px #a9a9a9');
      $('div.stopButton').css('background-color', '#000000');
      $('div.stopButton').css('border', 'solid 1px #000000');
      // change color of playButton
      $('div.playButton').css('border-color', '#000000 transparent transparent');
    })
    .live('mouseout', function(event) {
      $(this).css('border', 'solid 1px #a9a9a9');
      $('div.stopButton').css('background-color', '#000000');
    });


/*
  $("button.noteAllOn").live('click', (function(event) {
    var activeTrackCount = 0;
    for(var i= 1; i < track.nextTrackNo; i++ ) {
      if(typeof track[i] == 'object' ) activeTrackCount++;
    }
    if( track.nextTrackNo > 1 && activeTrackCount > 0) {
      if(event.target.tagName == 'INPUT') {
        var trackNo = $(this).attr('trackNo');
      }
      for(var i=1; i < track.nextTrackNo; i++) {
        $("span.playStatus_" + i).addClass("label-warning");
      }
      track.noteAllOn(0, 0);
    } else {
      alert('Drop audio file in the gray filed.');
    }
  }));
  $("button.noteAllOff").live('click', (function(event) {
        var activeTrackCount = 0;
    for(var i= 1; i < track.nextTrackNo; i++ ) {
      if(typeof track[i] == 'object' ) activeTrackCount++;
    }
    if( track.nextTrackNo > 1 && activeTrackCount > 0 ) {
      if(event.target.tagName == 'INPUT') {
        var trackNo = $(this).attr('trackNo');
      }
      for(var i=0; i < track.nextTrackNo; i++) {
        $("span.playStatus_" + i).removeClass("label-warning");
      }
      track.noteAllOff(0, 0);
    } else {
          alert('Drop audio file in the gray filed.');
    }
  }));
*/
  
 $('.tracks').delegate('.mute, .solo', 'click', function(event){
   var type = $(this).hasClass('mute') ? 'mute' : 'solo',
       active = $(this).hasClass('badge-warning'),
       trackNo = $(this).attr('trackNo');
   switch (type){
    case 'mute':
     $(this).toggleClass('badge-warning');
     $(this).siblings().removeClass('badge-warning');
     if( active ) {
       track.unmute(trackNo, '.mute');
     } else {
       track.mute(trackNo, '.mute');
     }
     break;
    case 'solo':
     $(this).toggleClass('badge-warning');
     $(this).siblings().removeClass('badge-warning');
     if( active ) {
       track.unsolo(trackNo);
     } else {
       track.solo(trackNo);
     }
     break;
   }
  });

 $('.master').delegate('.mute_all', 'click', function(event){
   var active = $(this).hasClass('badge-warning'),
       trackNo = 0;
     $(this).toggleClass('badge-warning');
     $(this).siblings().removeClass('badge-warning');
     if( active ) {
       track.unmuteAll();
     } else {
       track.muteAll();
     }
  });

});
  
var WindowClickEvent = function() {
  return {
    addTrack : function(section, contents){
      $('div.' + section)
        .append( contents )
        .find('.fader_volume')
        .bind('slide', function(event, ui){
          var trackNo = $(this).attr('trackNo');
          var value = ui.value;
          track.setGainValue(trackNo, value);
        })
        .slider({
          orientation: "vertical",
          range: "min", 
          animate: true,
          value: 0.8,
          min: 0,
          max: 1,
          step: 0.1
        })
        .end()
        .find('.fader_pan')
        .slider({
          value: 0,
          min: -1,
          max: 1,
          step: 0.1
        })
        .bind('slide', function(event, ui){
          var trackNo = $(this).attr('trackNo');
          var value = ui.value;
          track.setPanner(trackNo, value, 0, 0);
        })
        .bind('dblclick', function(event){
          var trackNo = $(this).attr('trackNo');
          $(this).slider('option', 'value', 0);
          track.setPanner(trackNo, 0, 0,  0);
        })
        .end()
        .find('.noteOn')
        .bind('click', function(event){
          var trackNo = $(this).attr('trackNo');
          $("span.playStatus_" + trackNo).addClass("label-warning");
          track.noteOn(trackNo, 0);
        })
        .end()
        .find('.noteOff')
        .bind('click', function(event){
          var trackNo = $(this).attr('trackNo');
          $("span.playStatus_" + trackNo).removeClass("label-warning");
          track.noteOff(trackNo, 0);
        })
        .end()
        .find('.deleteTrack')
        .bind('click', function(event){
          var trackNo = $(this).attr('trackNo');
          track.delete(trackNo);
          $("div#" + trackNo).remove();
         })
        .end()
        .find(".knobEqL")
        .bind('mousedown', function(event) {
          var trackNo = $(this).attr('trackNo');
          knobControl.setValAll(track.getEqVal(trackNo, 'eqL'));
          var rect = event.target.getBoundingClientRect();
          knobControl.setMouseDownStatus( true );
          knobControl.mouseDown(event, rect);
        })
        .bind('mousemove', function(event){
          var trackNo = $(this).attr('trackNo');
          var rect = event.target.getBoundingClientRect();
          var knobVal = knobControl.mouseMove(event, rect);
          if(typeof knobVal != 'undefined') {
            $(this).css("-webkit-transform", "rotate(" + knobVal.val +"deg)" );
            track.setEqVal(trackNo, 'eqL', knobVal);
          }
        }).bind('mouseout', function(){
          knobControl.setMouseDownStatus( false );
        }).bind('mouseup', function(){
          knobControl.setMouseDownStatus( false );
        })
        .end()
        .find(".knobEqH")
        .bind('mousedown', function(event) {
          var trackNo = $(this).attr('trackNo');
          knobControl.setValAll(track.getEqVal(trackNo, 'eqH'));
          var rect = event.target.getBoundingClientRect();
          knobControl.setMouseDownStatus( true );
          knobControl.mouseDown(event, rect);
        })
        .bind('mousemove', function(event){
          var trackNo = $(this).attr('trackNo');
          var rect = event.target.getBoundingClientRect();
          var knobVal = knobControl.mouseMove(event, rect);
          if(typeof knobVal != 'undefined') {
            $(this).css("-webkit-transform", "rotate(" + knobVal.val +"deg)" );
            track.setEqVal(trackNo, 'eqH', knobVal);
          }
        }).bind('mouseout', function(){
          knobControl.setMouseDownStatus( false );
        }).bind('mouseup', function(){
          knobControl.setMouseDownStatus( false );
        })
        .end()
        ;
    },
    addEventSlider: function(contents) {
      $('div.eventslider')
        .append( contents )
        .bind('slide', function(event, ui) {
          track.setSliderPosition(ui.value);
          /*
          // move during playing
          // realtime seek. but a bit heavy to cpu
          if( track[0].nowPlaying == true ) {
            track.noteAllOff();
            track.noteAllOn();
          }         
          */
        })
        .bind('mouseup', function(event, ui){
          console.log('SliderPos: ', track[0].position.slider);
          // move during playing
          if( track[0].nowPlaying == true ) {
            track.noteAllOff(false);
            track.noteAllOn();
          }         
        })
        .slider({
          orientation: "hirizontal",
          range: "min", 
          animate: true,
          value: 0,
          min: 0,
          max: 860,
          step: 0.1
        })
        .end()
      ;
    },

    addEventTrack : function(section, trackNo, contents){
      $('div.' + section)
        .append( contents )
        .find('div.eventPosition')
        .bind('mouseover', function(event) {
          $(this).css('cursor', 'pointer');
        })
        .bind('mousedown', function(event) {
          var trackNo = $(this).attr('trackno');
          track[trackNo].eventElement = {
              'mousedown' : true,
              'X': event.clientX,
              'Y': event.clientY
            };
            $(this).css('cursor', 'pointer');
            event.stopImmediatePropagation();
        })
        .bind('mouseup', function(event) {
          var trackNo = $(this).attr('trackno');
          if(track[trackNo].eventElement.mousedown == true) {
            track[trackNo].eventElement.mousedown = false;
            console.log('mouseUP');
            if( track[0].nowPlaying == true ) {
              track.noteAllOff(false);
              track.noteAllOn();
            }
          }
        })
        .bind('mouseout', function(event) {
          var trackNo = $(this).attr('trackno');
          track[trackNo].eventElement.mousedown = false;
        })
        .bind('mousemove', function(event){
          var trackNo = $(this).attr('trackno');
          if(track[trackNo].eventElement.mousedown == true) {
            $(this).css('cursor', 'pointer');
            //console.log('dragging', event.clientX, move, now);

            // Rigth <-> Left
            var rlNow = parseInt( $(this).css('left').replace('px', '') );
            var rlMove = event.clientX - track[trackNo].eventElement.X;
            track[trackNo].eventElement.X = event.clientX;
            var rlPosition = rlNow + 1.0 * rlMove;
            if(rlPosition >= 0) {
              $(this).css('left', rlPosition + 'px');
              track[trackNo].startOffset = parseFloat(rlPosition);
            }
            
            // Up <-> Down
            //var posiY = event.clientY - event.offsetY; // Position of this Div
/*
            var udNow = parseInt( $(this).css('top').replace('px', '') );
            var udMove = event.clientY - track[trackNo].eventElement.Y;
            if(Math.abs(udMove) > 20) {
              track[trackNo].eventElement.Y = event.clientY;
              var udPosition = udNow + 1.0 * udMove;
              $(this).css('top', udPosition + 'px');
            }
*/

            
          }
          


          event.stopImmediatePropagation();
        })
/*
        .bind('dblclick', function(event){
          var trackNo = $(this).attr('trackno');
          console.log(track[trackNo].eventElement.dblclick);

          if(track[trackNo].eventElement.dblclick == true) {
            console.log('remove', trackNo );
            $(this).removeClass('grabElement');
            track[trackNo].eventElement.dblclick = false;
          } else {
            console.log('add', trackNo);
            $(this).addClass('grabElement');
            track[trackNo].eventElement.dblclick = true;
          }


//          console.log(this, event);
//          console.log('dblclicked!!');
          event.stopImmediatePropagation();
        })
*/
        ;

      var widthOfEventTrack = 60; 
      // add EventTrack
      var sectionName = 'canvasEventArea_' + trackNo;
      var contentLength = track[trackNo].duration * 1; // 1 sec/px
      var element = document.getElementById(sectionName);
      var newElement = document.createElement('span');
      newElement.innerHTML = '<canvas id="canvas_' + trackNo + '" trackNo="'+ trackNo +'"width="'+ contentLength + '" height="'+ widthOfEventTrack +'"></canvas>';
      element.insertBefore(newElement);
      var canvas = document.getElementById("canvas_" + trackNo);
      var canvasCtx = canvas.getContext("2d");

      canvasCtx.lineWidth = 1; 
      
      // draw content length
      canvasCtx.save();
      canvasCtx.beginPath();
      //canvasCtx.fillStyle = 'rgb(102, 102, 102)';
      //canvasCtx.fillRect(0, 0, contentLength, widthOfEventTrack);

      /* グラデーション領域をセット */
      // http://www.html5.jp/canvas/sample/sample7.html
      var grad  = canvasCtx.createLinearGradient(0, 0, 0, widthOfEventTrack);
      /* グラデーション終点のオフセットと色をセット */
      grad.addColorStop(0,  'rgb(230, 230, 230)');
      grad.addColorStop(0.5,'rgb(200, 200, 200)');
      grad.addColorStop(1,  'rgb(170, 170, 170)');
      /* グラデーションをfillStyleプロパティにセット */
      canvasCtx.fillStyle = grad;
      /* 矩形を描画 */
      canvasCtx.rect(0, 0, contentLength, widthOfEventTrack);
      canvasCtx.fill();
      
      canvasCtx.restore();
      canvasCtx.save();

      // write event track name
      var trackName= track[trackNo].name.replace('.mp3', ' ').replace('.wav', ' ').replace('.ogg', ' ').replace('1901_', ' ').replace('_', ' ');
      canvasCtx.font = 'bold 10pt Arial';
      canvasCtx.fillStyle = 'rgb(100, 100, 100)';
      canvasCtx.fillText(trackName, 0, 13);
      
      // draw center line
      canvasCtx.save();
      var eventTrackCenter = widthOfEventTrack / 2; 
      canvasCtx.beginPath();
      canvasCtx.strokeStyle = 'rgb(100, 100, 100)'; // 'rgb(229, 229, 229)';
      canvasCtx.moveTo(0, eventTrackCenter);
      canvasCtx.lineTo(contentLength, eventTrackCenter);
      canvasCtx.stroke();
      canvasCtx.save();

      // draw audio data
      canvasCtx.save();
      var yMultiple = 40;
      var xTicks = contentLength / track[trackNo].frequencyData.L.length;

      var xGrid = 0;
      canvasCtx.beginPath();
      for( var i=0; i<track[trackNo].frequencyData.L.length; i++ ) {
        xGrid = i * xTicks;
        canvasCtx.strokeStyle = 'rgb(100, 100, 100)'; // 'rgb(255, 255, 255)';
        canvasCtx.moveTo(xGrid, eventTrackCenter);
        canvasCtx.lineTo(xGrid, eventTrackCenter - track[trackNo].frequencyData.L[i] * yMultiple);
        canvasCtx.stroke();

        canvasCtx.moveTo(xGrid, eventTrackCenter);
        canvasCtx.lineTo(xGrid, eventTrackCenter + track[trackNo].frequencyData.R[i] * yMultiple);
        canvasCtx.stroke();
      }
      canvasCtx.save();

      // change width of Event Track
      $('div.' + sectionName).css("width", contentLength + 4 + "px" );
      
    },

    moveSliderPosition: function(name, value) {
      $(name).slider("value", value);
    }
    
  };
};

var windowClickEvent = new WindowClickEvent();


