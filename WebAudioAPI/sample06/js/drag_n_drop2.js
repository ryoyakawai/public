var drop = document.getElementById("dropArea");

var sound = { };
sound.isPlaying = false;
drop.addEventListener(
  "drop", 
  function(event) {
    event.preventDefault();
    drop.style.backgroundColor = "#ccc";
    //
    if( sound.isPlaying == true) {
      alert('only one audio file playing is allowd.');
      return;
    }
    
    var filelist = event.dataTransfer.files;

    for (var i = 0; i<filelist.length; i++) {
      if( window.FileReader && (filelist[i].type.match("audio/*"))) {
        var _filelist = filelist[i];
        var trackNo = track.add(_filelist.name);
        _filelist.trackNo = trackNo;

        var reader= new FileReader();

        reader.onload = (function(_filelist) {
          return function(event) {
            track.setBuffer(_filelist.trackNo, event.target.result);
            track.setSourceLoop(_filelist.trackNo, true);
            // add track ui
            _filelist.sizeMB = Math.round( _filelist.size / 10000 ) / 100;
            var trackUi = TrackElement.getTrackTemplate(_filelist.trackNo, _filelist);
            var elem = document.createElement('div');

            elem.innerHTML = trackUi;
            objBody.appendChild(elem);
            // add track ui 
          };
        })(_filelist);
        reader.readAsArrayBuffer(_filelist);
      } else {
        alert('could not play that audio');
      }
    }
}, true);

function draw(data){
  sound.ctx.beginPath();
  sound.ctx.fillStyle = "black";
  sound.ctx.rect(0,0,sound.canvas.width,sound.canvas.height);
  sound.ctx.fill();
  var value;
  sound.ctx.beginPath();
  sound.ctx.moveTo(0,-999);
  for (var i=0; i<data.length; ++i){
    value = data[i] - 128 + sound.canvas.height / 2;
    sound.ctx.lineTo(i,value);
  }
  sound.ctx.moveTo(0,999);
  sound.ctx.closePath();
  sound.ctx.strokeStyle = "gray";
  sound.ctx.stroke();
}

function stop(){
  sound.isPlaying = false;
  track.noteOff(1);
}

// ドラッグする要素をドラッグするとき
drop.addEventListener("dragenter", function(event) {
    event.preventDefault();
    drop.style.backgroundColor = "#ecc";
}, true);

// ドロップ領域を出たとき
drop.addEventListener(
  "dragleave", 
  function(event) {
    event.preventDefault();
    drop.style.backgroundColor = "#ccc";
  }, true);

// このイベントはドラッグ＆ドロップする要素には必須
drop.addEventListener(
  "dragover", 
  function(event) {
    event.preventDefault();
  },
  true);

// ドロップ領域にドロップされたとき
window.addEventListener(
  "drop", 
  function(event) { event.preventDefault();},
  true
);

// ドロップ領域に入ったとき
window.addEventListener(
  "dragenter", 
  function(event) { event.preventDefault(); }, 
  true
);

// ドロップ領域を出たとき
window.addEventListener(
  "dragleave", 
  function(event) { event.preventDefault();}, 
  true
);

// このイベントはドラッグ＆ドロップする要素には必須
window.addEventListener(
  "dragover", 
  function(event) { event.preventDefault();}, 
  true
);
