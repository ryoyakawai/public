// create context
var context = new webkitAudioContext();

// define basicTrack
var basicTrack = function(name, number) {
  this.volume = context.createGainNode();
  this.panner = context.createPanner();
  this.panner.setPosition(0, 0, 0);
  
  this.number = number;
  if(number == 0) { // master
    this.name = 'master';
    this.type = 'master';
    this.volume.gain.value = 0.3;
    this.volume.connect(this.panner);
    this.panner.connect(context.destination);    
  } else {
    this.name = name;
    this.type = 'TEST';
    this.volume.gain.value = 0.5;
    this.source = context.createBufferSource();
  }
};  

// must create construct for prototype
var Track = function(){
  this.nextTrackNo = 0;
};
Track.prototype = {
  add: function(name) {
    var trackNo = this.nextTrackNo++;
    track[trackNo] = new basicTrack(name, trackNo);
    return trackNo;
  },
  setBuffer: function(trackNo, buffer) {
    track[trackNo].source.buffer = context.createBuffer(buffer, false);
  },
  setGainValue: function(trackNo, value) {
    track[trackNo].volume.gain.value = value;
  },
  setSourceLoop: function(trackNo, value) {
    track[trackNo].source.loop = value;
  },
  setPanner: function(trackNo, x, y, z) {
    track[trackNo].panner.setPosition(x, y, z);
  },
  noteOn: function(trackNo) {
    track[trackNo].source.connect(track[trackNo].volume);
    track[trackNo].volume.connect(track[trackNo].panner);
    track[trackNo].panner.connect(track[0].volume);
    track[trackNo].source.noteOn(0);
  },
  noteOff: function(trackNo) {
    track[trackNo].source.disconnect();
  },
  noteAllOn: function() {
    for(var i= 1; i < this.nextTrackNo; i++ ) {
      this.noteOn(i);
    }
  },
  noteAllOff: function() {
    for(var i= 1; i < this.nextTrackNo; i++ ) {
      this.noteOff(i);
    }
  }
};

// create track
var track = new Track();


// create Master
track.add('master');

// add mastertrack ui
var elem = document.createElement('div');
var trackUi = TrackElement.getMasterTrack();
elem.classList.add('tracks');
elem.innerHTML = trackUi;
var objBody = document.getElementsByTagName("body").item(0);
objBody.appendChild(elem);
// add mastertrack ui 
