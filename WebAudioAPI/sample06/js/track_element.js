var TrackElement = function() {
  var colorCycle = {
    '0': '#FFFFF0',
    '1': '#FFFAFA'
  };

  return {
    getMasterTrack: function() {
      var trackTemplate =
        '<div id="0" name="0" style="background-color:#F5F5F5">'+
        '<a class="playStatus_0">[-]</a> <B>MasterTrack</B> <BR>'+
        //'<input type="checkbox" class="active" name="1"><b>active</b>'+
        '<br>' +
        '<div class="trackno"></div>'+
		      '<b>Pan :</b>'+
		      '<input type="range" class="range_pan" trackNo="0" name="volume" min="-1" max="1" value="0" step="0.1"><BR>'+
		      '<b>Volume :</b>'+
		      '<input type="range" class="range_volume" trackNo="0" name="volume" min="0" max="1" value="0.8" step="0.1">'+
		      //'<input type="text" class="text_range_amplitude" value="0">'+
        '<input type="button" class="noteAllOn" trackNo="0" name="noteAllOn" value="noteAllOn">' + 
        '<input type="button" class="noteAllOff" trackNo="0" name="noteAllOff" value="noteAllOff">' + 
        '</div>'+
        ''+
		      '<BR>';
      
      return trackTemplate;
    },
    
    getTrackTemplate: function(trackNo, fileInfo) {
      var trackColor = colorCycle[trackNo%2];
      var trackTemplate =
        '<div id="'+ trackNo +'" name="'+ trackNo +'" style="background-color:'+ trackColor +'">'+
        '<a class="playStatus_' + trackNo + '">[-]</a> <B>TrackNo:'+ trackNo + '</B> (' + fileInfo.name + ' : ' + fileInfo.sizeMB + ' MB)<BR>'+
        //'<input type="checkbox" class="active" name="1"><b>active</b>'+
        '<br>' +
        '<div class="trackno"></div>'+
		      '<b>Pan :</b>'+
		      '<input type="range" class="range_pan" trackNo="'+ trackNo +'" name="volume" min="-1" max="1" value="0" step="0.1"><BR>'+
		      '<b>Volume :</b>'+
		      '<input type="range" class="range_volume" trackNo="'+ trackNo +'" name="volume" min="0" max="1" value="0.8" step="0.1">'+
		      //'<input type="text" class="text_range_amplitude" value="0">'+
        '<input type="button" class="noteOn" trackNo="'+ trackNo +'" name="noteOn" value="noteOn">' + 
        '<input type="button" class="noteOff" trackNo="'+ trackNo +'" name="noteOff" value="noteOff">' + 
        '</div>'+
        ''+
		      //'<BR><BR>'+
        '';

      return trackTemplate;
    }
  };

}();
  
