$(function() {
  $("input.stop").hide();
  $("p.playing").hide();
}
 );

$("input.stop").click(function() {
    stop();
    $("input.stop").hide();
    $("p.playing").hide(); 
    $("p.stopped").show();
  });

$("input.range_volume").live('change', (function(event) {
  if(event.target.tagName == 'INPUT') {
    var trackNo = $(this).attr('trackNo');
    var value = $(this).attr('value');
  }
  track.setGainValue(trackNo, value);
}));
$("input.range_pan").live('change', (function(event) {
  if(event.target.tagName == 'INPUT') {
    var trackNo = $(this).attr('trackNo');
    var value = $(this).attr('value');
  }
  track.setPanner(trackNo, value, 0,  0);
}));
$("input.range_pan").live('dblclick', (function(event) {
  if(event.target.tagName == 'INPUT') {
    var trackNo = $(this).attr('trackNo');
  }
  $('input.range_pan').attr('value', 0);
  track.setPanner(trackNo, 0, 0,  0);
}));
$("input.noteOn").live('click', (function(event) {
  if(event.target.tagName == 'INPUT') {
    var trackNo = $(this).attr('trackNo');
  }
  $("a.playStatus_" + trackNo).html("<font color=\"blue\">[>]</font>");
  track.noteOn(trackNo, 0);
}));
$("input.noteOff").live('click', (function(event) {
  if(event.target.tagName == 'INPUT') {
    var trackNo = $(this).attr('trackNo');
  }
  $("a.playStatus_" + trackNo).html("[=]");
  track.noteOff(trackNo, 0);
}));
$("input.noteAllOn").live('click', (function(event) {
  if( track.nextTrackNo > 1 ) {
    if(event.target.tagName == 'INPUT') {
      var trackNo = $(this).attr('trackNo');
    }
    for(var i=0; i < track.nextTrackNo; i++) {
      $("a.playStatus_" + i).html("<font color=\"blue\">[>]</font>");        
    }
    track.noteAllOn(0, 0);
  } else {
    alert('Drop audio file in the gray filed.');
  }
}));
$("input.noteAllOff").live('click', (function(event) {
  if( track.nextTrackNo > 1 ) {
    if(event.target.tagName == 'INPUT') {
      var trackNo = $(this).attr('trackNo');
    }
    for(var i=0; i < track.nextTrackNo; i++) {
      $("a.playStatus_" + i).html("[=]");
    }
    track.noteAllOff(0, 0);
  } else {
    alert('Drop audio file in the gray filed.');
  }
}));


