/**
 * based on T'SoundSystem for JavaScript
 */

/**
 * MidiChannel base prototype
 *
 * Abstract MIDI device.
 */
function MidiChannel () {

  return {
    init: function() {
      this.events = [];
      this.outTo = 'MIDIDevice';
      //
      this.EVENT_TYPE_MIDI = 0;
      this._NOTE_FREQUENCY_TABLE = [];
      this._MIDI_EVENT_SYSEX = 0xf0;
      this._MIDI_EVENT_MTC_QUOTER_FRAME_MESSAGE = 0xf1;
      this._MIDI_EVENT_SONG_POSITION_POINTER = 0xf2;
      this._MIDI_EVENT_SONG_SELECT = 0xf3;
      this._MIDI_EVENT_TUNE_REQUEST = 0xf6;
      this._MIDI_EVENT_EOX = 0xf7;
      this._MIDI_EVENT_TIMING_CLOCK = 0xf8;
      this._MIDI_EVENT_START = 0xfa;
      this._MIDI_EVENT_CONTINUE = 0xfb;
      this._MIDI_EVENT_STOP = 0xfc;
      this._MIDI_EVENT_ACTIVE_SENCING = 0xfe;
      this._MIDI_EVENT_SYSTEM_RESET = 0xff;
      this._MIDI_EVENT_SYSTEM_LENGTH = [ -1, 2, 3, 2, -1, -1, 1, 1 ];
    },

    /**
     *
     * set Out device to WebMIDILink
     *
     */
    setOutToWebMIDILink: function(){
      this.outTo = 'WebMIDILink';
    },

    /**
     * Create MIDI event from an array.
     * @param data an array of MIDI data (TypedArray, Array, or array)
     * @return object
     *      type: event type
     *      midiData: MIDI data of array
     */
    createEvent: function (data) {
      // TODO: Should we check data type and payload here?
      return {
        type: this.EVENT_TYPE_MIDI,
        midiData: data
      };
    },
    
    /**
     * Process MIDI event object containing MIDI events. User can use this function
     * to send MIDI events to virtual MIDI devices which inherit this prototype.
     * Each prototype which inherits this prototype can overwrite this function.
     * But, usually overwriting following process* functions is recommended because
     * splitting raw MIDI byte stream into each MIDI event is a little complicated
     * and mishandling causes critical error to stop event handling.
     * @param events MIDI event object to process
     * @return the number of processed events
     */
    processEvents: function (events) {
      // Append MIDI events to internal array.
      // TODO: Expand running status here.
      for (var i = 0; i < events.midiData.length; i++) {
        if ((events.midiData[i] < 0x00) || (0xff < events.midiData[i])) {
          Log.getLog().warn("MIDI: midiData[" + i + "] is out of range, " +
                            events.midiData[i]);
          events.midiData[i] &= 0xff;
        }
        if (events.midiData[i] >= 0xf8) {
          if (this._MIDI_EVENT_SYSTEM_RESET == events.midiData[i])
            this.processSystemReset();
          else
            this.processSystemRealtimeMessage([ events.midiData[i] ]);
        } else
          this.events.push(events.midiData[i]);
      }
      
      for (var done = 0; 0 != this.events.length; done++) {
        var event = this.events[0];
        var type = event >> 4;
        var channel = event & 0x0f;
        switch (type) {
         case 0x8:  // note off
          if (this.events.length < 3)
            return done;
          this.processNoteOff(channel, this.events[1], this.events[2]);
          this.events.splice(0, 3);
          break;
         case 0x9:  // note on
          if (this.events.length < 3)
            return done;
          if (0 == this.events[2]) {  
            this.processNoteOff(channel, this.events[1], this.events[2]);
          } else {
            this.processNoteOn(channel, this.events[1], this.events[2]);
          }
          this.events.splice(0, 3);
          break;
         case 0xa:  // key pressure
          if (this.events.length < 3)
            return done;
          this.processKeyPressure(
            channel, this.events[1], this.events[2]);
          this.events.splice(0, 3);
          break;
         case 0xb:  // control change
          if (this.events.length < 3)
            return done;
          this.processControlChange(
            channel, this.events[1], this.events[2]);
          this.events.splice(0, 3);
          break;
         case 0xc:  // program change
          if (this.events.length < 2)
            return done;
          this.processProgramChange(channel, this.events[1]);
          this.events.splice(0, 2);
          break;
         case 0xd:  // channel pressure
          if (this.events.length < 2)
            return done;
          this.processChannelPressure(channel, this.events[1]);
          this.events.splice(0, 2);
          break;
         case 0xe:  // pitch bend
          if (this.events.length < 3)
            return done;
          var bend = (this.events[1] << 7) | this.events[2];
          if (bend > 8191)
            bend = bend - 16384;
          this.processPitchBend(channel, bend);
          this.events.splice(0, 3);
          break;
         case 0xf:  // system messages
          if (this._MIDI_EVENT_SYSEX == event) {
            // system exclusive
            for (var sysexLength = 1; sysexLength < this.events.length;
                 sysexLength++) {
              if (this._MIDI_EVENT_EOX ==
                  this.events[sysexLength])
                break;
            }
            if (sysexLength == this.events.length)
              return done;
            this.processSystemExclusive(this.events.splice(0,
                                                           sysexLength + 1));
          } else if (event < 0xf8) {
            // system common
            var systemLength =
              this._MIDI_EVENT_SYSTEM_LENGTH[event & 0x7];
            if (systemLength < 0) {
              Log.getLog().warn("MIDI: unknown system message " +
                                event.toString(16));
              systemLength = 1;
            }
            if (this.events.length < systemLength)
              return done;
            var systemMessage = this.events.splice(0, systemLength);
            this.processSystemCommonMessage(systemMessage);
          } else {
            Log.getLog().error("MIDI: unexpected realtime event");
          }
          break;
        }
      }
    },

    /**
     * Process note off event. This function should be overwritten.
     * @param ch channel
     * @param note note number
     * @param velocity key off velocity
     */
    processNoteOff: function (ch, note, velocity) {
      this.sendToWebMidiLink([0x80 + ch, note, velocity]);
    },

    /**
     * Process note on event. This function should be overwritten.
     * @param ch channel
     * @param note note number
     * @param velocity key on velocity
     */
    processNoteOn: function (ch, note, velocity) {
      this.sendToWebMidiLink([0x90 + ch, note, velocity]);
    },

    /**
     * Process key pressure event. This function can be overwritten.
     * @param ch channel
     * @param note note number
     * @param pressure key pressure
     */
    processKeyPressure: function (ch, note, pressure) {
      this.sendToWebMidiLink([0xa0 + ch, note]);
    },

    /**
     * Process control change event. This function can be overwritten.
     * @param ch channel
     * @param number control number
     * @param value control value
     */
    processControlChange: function (ch, number, value) {
      this.sendToWebMidiLink([0xb0 + ch, number, value]);
    },

    /**
     * Process program change event. This function can be overwritten.
     * @param ch channel
     * @param number channel number
     */
    processProgramChange: function (ch, number) {
      this.sendToWebMidiLink([0xc0 + ch, number]);
    },

    /**
     * Process program change event. This function can be overwritten.
     * @param ch channel
     * @param pressure channel pressure
     */
    processChannelPressure: function (ch, pressure) {
      this.sendToWebMidiLink([0xd0 + ch, pressure]);
    },

    /**
     * Process pitch bend event. This function can be overwritten.
     * @param ch channel
     * @param bend pitch bend (-8192 - 8191)
     */
    processPitchBend: function (ch, bend) {
      this.sendToWebMidiLink([0xe0 + ch, bend]);
    },

    /**
     * Process one system exclusive event. This function can be overwritten.
     * @param sysex a system exclusive event of array
     */
    processSystemExclusive: function (sysex) {
      this.sendToWebMidiLink.apply(this, sysex);
    },

    /**
     * Process one system common message event. This function can be overwritten.
     * @param common a system common message event of array
     */
    processSystemCommonMessage: function (common) {
      this.sendToWebMidiLink.apply(this, common);
    },

    /**
     * Process one system realtime message event. This function can be overwritten.
     * @param realtime a realtime message event of array
     */
    processSystemRealtimeMessage: function (realtime) {
      this.sendToWebMidiLink.apply(this, realtime);
    },

    /**
     * Process system reset message event. This function can be overwritten.
     */
    processSystemReset: function () {
      this.sendToWebMidiLink(0xff);
    },

    /**
     * Process meta data event. This function can be overwritten.
     * @param meta meta data event of array
     */
    processMetaData: function (meta) {
      this.sendToWebMidiLink.apply(this, meta);
    },
    
    /**
     * Send a sequence of MIDI data to the connected MIDI device.
     */
    sendToWebMidiLink: function (command) {
      var commands = ["midi"];
      var commands2 = new Array();
      for(var i=0; i<command.length; i++) {
        commands.push(command[i].toString(16));
        commands2.push(command[i]);
      }
      switch(this.outTo) {
       case 'MIDIDevice':
        o.send(commands2);
        break;
       case 'WebMIDILink':
        port.SendMessage(commands.join(","), "*"); // WebMIDILink
        break;
      }
    }
  };

}

