var holder = document.getElementById('holder');

holder.ondragover = function () {  this.className = 'hover'; return false; };
holder.ondragleave = function () { this.className = ''; return false; };
holder.ondragend = function () { this.className = ''; return false; };
holder.ondrop = function (e) {

  checkMidiConnection();
  console.log('aaaa');
  
  this.className = '';
  e.preventDefault();
  var file = e.dataTransfer.files[0],
  reader = new FileReader();
  reader.onload = function (event) {
    smfParser.play(event.target.result);
  };
  reader.onerror = function (event) {
    alert("Error: " + reader.error );
  };
  reader.readAsArrayBuffer(file);
  return false;
};
