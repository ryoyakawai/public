var smfParser = new SmfParser();
smfParser.init();
  
var midi = new MidiChannel();
midi.init();

smfParser.setDevice(smfParser.TRACK_DEFAULT, midi);
smfParser.setMasterChannel(new TimerMasterChannel(1));
//smfParser.setMasterChannel(new TimerMasterChannel(TimerMasterChannel.MODE_DEFAULT));

function checkMidiConnection() {
  if(o == null) {
    createMIDIConnection(deviceSelector, setOutToWebMIDILink);
  }
}

function midiError() {
  console.log('MIDI Device may not be connected.');
}


var elm = document.getElementById('deviceSelector');
var evt = document.createEvent("MouseEvents");
//console.log(elm, evt);
//evt.initEvent("click", false, true);
//elm.dispatchEvent(evt);


var deviceSelector = function(deviceList){
  deviceList.push({'name': 'WebMIDILink','manufacturor': 'g200kg'});
  var selectedDevice = showModalDialog("deviceSelector.html", deviceList,"dialogWidth:300px;dialogHeight:240px");
  var name = selectedDevice;
  if(selectedDevice=='wml') {
    name = 'WebMIDILink';
  } else {
    name = deviceList[selectedDevice].name;
  }
  $('input.mididevicename').attr('value', name);
  $("div.midiStatus").css('background-color', '#8fbc8f');
  return selectedDevice;
};

var port = null;
var setOutToWebMIDILink = function() {
  midi.setOutToWebMIDILink();
  port = new Synth();
  //port.Load("http://www.g200kg.com/en/docs/gmplayer/");
  port.LoadAsFrame("http://www.g200kg.com/en/docs/gmplayer/");
};

function Synth() {
  this.sy = null;
  this.Load = function(url) {
    this.sy = window.open(url, "sy1", "width=900,height=670,scrollbars=yes,resizable=yes");
  };
  this.LoadAsFrame = function(url) {
    $('div.wml').html('<iframe style="margin: 10px 20px 0px 20px; padding:5px; border:1px black solid; width:80%; height:400px;" scrolling="no" frameborder="0" id="wml" src="' + url + '"></iframe>');
    this.sy = document.getElementById("wml").contentWindow;
  };
  this.SendMessage = function(s) {
    this.sy.postMessage(s, "*");
  };
}


$(document).ready(function() {
});

function launchWindow(id) {
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
		
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect
		$('#mask').fadeIn(1000);
		$('#mask').fadeTo("slow",0.8);
		
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height());
		$(id).css('left', winW/2-$(id).width()/2);
		
		//transition effect
		$(id).fadeIn(2000); 
}




$(document).ready(function(){

  
		//Put in the DIV id you want to display
		launchWindow('#dialog1');
		
		//if close button is clicked
		$('.window #close').click(function () {
		  $('#mask').hide();
		  $('.window').hide();
		});
		
		//if mask is clicked
		$('#mask').click(function () {
		  $(this).hide();
		  $('.window').hide();
		});
		
		$(window).resize(function () {
		  var box = $('#boxes .window');
    
    //Get the screen height and width
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    
    //Set height and width to mask to fill up the whole screen
    $('#mask').css({'width':maskWidth,'height':maskHeight});
    
    //Get the window height and width
    var winH = $(window).height();
    var winW = $(window).width();
    
    //Set the popup window to center
    box.css('top',  winH/2 - box.height()/2);
    box.css('left', winW/2 - box.width()/2);
		});



  
  $('input.midiButton').click(function(){
    switch($(this).attr('status')){
     case 'off':
      createMIDIConnection(deviceSelector, setOutToWebMIDILink);
      break;
    }
  });
});




