var m = null;   // m = MIDIAccess object for you to make calls on
var o = null; 
var popUpDeviceSelector = null;
var setOutToWebMIDILink = null;
var onerrorcallback = null;

function createMIDIConnection(deviceSelector, setOutToWebMIDILink) {
  popUpDeviceSelector = deviceSelector;
  onerrorcallback = setOutToWebMIDILink = setOutToWebMIDILink;
  navigator.requestMIDIAccess( onsuccesscallback, onerrorcallback );

}

function onsuccesscallback( access ) {
  m = access;
  
  var inputs = getInputPorts(m);   // inputs = array of MIDIPorts
  var outputs = getOutputPorts(m);  
  var i = m.getInput( inputs[0] );    // grab first input device.  You can also getInput( index );

  if(i==null) {
    onerrorcallback();
    return;
  }
  i.onmessage = onMIDIMessage;
  //midiConnected(inputs[0].name);

  var selectedDevice = popUpDeviceSelector(outputs);
  console.log(selectedDevice);

  if(selectedDevice=='wml') {
    setOutToWebMIDILink();
  } else {
    o = m.getOutput( outputs[selectedDevice] );
  }
};

function getInputPorts() {
  return m.getInputs();
}

function getOutputPorts() {
  return m.getOutputs();
}

function onMIDIMessage( event ) {
  var data = event.data;
  var data16 = [ event.data[0].toString(16), event.data[1].toString(16), event.data[2].toString(16)];
  
  switch(data16[0]) {
   case '90':
    webAudio.noteOn(data[1]);
    if(data[2]==0) webAudio.noteOff(data[1]);
    console.log(data[1]);
    break;
    case '80':
    webAudio.noteOff(data[1]);
    break;
    default:
    console.log('Not Supported yet!');
    break;
  }
  
}

