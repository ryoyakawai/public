var Rtc = function() {

  return{
    init: function(){
      this.localMediaStream=null;
      this.video=null;
      this.intervalId = null;
      this.imgHeight = 100;
      this.imgWidth = 132;
      this.count= { 'all': 0, 'virtical': 0, 'horizontal': 0 };
      this.browser='Webkit';
      if( typeof navigator.webkitGetUserMedia == 'undefined' ) {
		      this.browser='NonWebkit';
      }
    },

    streamOn: function() {
      self=this;
	     navigator.webkitGetUserMedia({video: true, audio: true}, this.gotStream, onFailSoHard);
      console.log(this);

      var onFailSoHard = function(e) {
        console.log('Reeeejected!', e);
      };
    },

    gotStream: function(localMediaStream) {
      self.localMediaStream = localMediaStream;
			   self.video = document.querySelector('video');
			   self.video.src = window.webkitURL.createObjectURL(self.localMediaStream);
      console.log(self);
    },

    streamOff: function(){
      switch( this.browser ) {
       case 'Webkit':
        this.localMediaStream.stop();
        break;
      }
      console.log(this.localMediaStream.stop());
      clearInterval(this.intervalId);
    },
    
    getSnapShot: function() {
      if(this.localMediaStream==null) {
        alert('Start video streaming before take a snapshot.');
        return false;
      }

      self = this;
      this.intervalId = setInterval(function() {
        if (this.intervalId) {
          clearInterval(this.intervalId);
          this.intervalId = null;
          return;
        }

        self.canvas = document.getElementById('photo');
        self.canvas.width = self.video.videoWidth;
        self.canvas.height = self.video.videoHeight;

        self.context = self.canvas.getContext('2d');
        self.context.drawImage(self.video, 0, 0);

        self.gallery = document.getElementById('gallery');

        var img = document.createElement('img');
        img.src = self.canvas.toDataURL('image/webp');

        self.count.all++;
        self.count.horizontal++;
        if(self.count.all % 10 == 0) {
          self.count.virtical++;
          self.count.horizontal=0;
        }
        if(self.count.virtical == 4) self.count.virtical = 0;

        img.style.top = 48 + self.imgHeight * self.count.virtical + 'px';
        img.style.left = 10 + self.imgWidth * self.count.horizontal + 'px';

        self.gallery.appendChild(img);

      }, 150);
    }
    
  };
    
};

var rtc = new Rtc();
rtc.init();
$('input.snapshot').hide();
$('span.live').hide();

$("input.streamOn").click(function(){
  var status = $('input.streamOn').val();
  switch(status) {
   case 'On':
    $('input.streamOn').attr('value', 'Off');
    $('input.snapshot').show();
    $('span.live').show();
    rtc.streamOn();
    break;
    case 'Off':
    rtc.streamOff();
    $('input.streamOn').attr('value', 'On');
    $('input.snapshot').hide();
    $('span.live').hide();
    break;
  }
});
$("input.snapShot").click(function(){
  if( rtc.getSnapShot() != false ) {
    $("input.snapShot").hide();
  }
});

