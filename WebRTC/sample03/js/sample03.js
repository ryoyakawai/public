var Rtc = function() {

  return{
    init: function(){
      this.nowPlaying = false;
      this.localMediaStream=null;
      this.video=null;
      this.audio=null;
      this.intervalId = null;
      this.imgHeight = 100;
      this.imgWidth = 132;
      this.count= { 'all': 0, 'virtical': 0, 'horizontal': 0 };
      this.browser='Webkit';
      this.audioContext = new webkitAudioContext();
      this.drawContext = null;
      this.analyserNodeCanvas = null;
      this.canvasMeter = null;

      this.analyserNode = null;
      
      if( typeof navigator.webkitGetUserMedia == 'undefined' ) {
		      this.browser='NonWebkit';
      }
    },

    streamOn: function() {
      this.nowPlaying = true;

      self=this;
	     navigator.webkitGetUserMedia({video: true, audio: true}, this.gotStream, onFailSoHard);

      var onFailSoHard = function(e) {
        console.log('Reeeejected!', e);
      };
    },

    gotStream: function(localMediaStream) {
      self.localMediaStream = localMediaStream;
			   self.video = document.querySelector('video');
			   self.video.src = window.webkitURL.createObjectURL(self.localMediaStream);

      self.audioStream = self.audioContext.createMediaStreamSource(localMediaStream);
      self.analyserNode = self.audioContext.createAnalyser();
      self.audioStream.connect(self.analyserNode);
      self.analyserNode.connect(self.audioContext.destination);
      
      // spectrum analyser
      self.analyserNode.fftSize = 2048;
      self.analyserNode.maxDecibles = 0;
      
      self.drawContext = document.getElementById('audioStream');
      self.analyserNodeCanvas = self.drawContext.getContext('2d');
      self.canvasMeter = new CanvasMeter(self.analyserNode, self.analyserNodeCanvas);
      
      function updateVisualiser(time) {
        self.canvasMeter.updateAnalyser(self.analyserNode, self.analyserNodeCanvas);
        self.canvasMeter.rafID = window.webkitRequestAnimationFrame( updateVisualiser );
      }
      updateVisualiser(0);
      // spectrum analyser
    },

    streamOff: function(){
      switch( this.browser ) {
       case 'Webkit':
        this.localMediaStream.stop();
        this.audioStream.disconnect();
        break;
      }
      clearInterval(this.intervalId);
    },
    
    getSnapShot: function() {
      if(this.localMediaStream==null) {
        alert('Start video streaming before take a snapshot.');
        return false;
      }

      self = this;
      this.intervalId = setInterval(function() {
        if (this.intervalId) {
          clearInterval(this.intervalId);
          this.intervalId = null;
          return;
        }

        self.canvas = document.getElementById('photo');
        self.canvas.width = self.video.videoWidth;
        self.canvas.height = self.video.videoHeight;

        self.context = self.canvas.getContext('2d');
        self.context.drawImage(self.video, 0, 0);

        self.gallery = document.getElementById('gallery');

        var img = document.createElement('img');
        img.src = self.canvas.toDataURL('image/webp');

        self.count.all++;
        self.count.horizontal++;
        if(self.count.all % 10 == 0) {
          self.count.virtical++;
          self.count.horizontal=0;
        }
        if(self.count.virtical == 4) self.count.virtical = 0;

        img.style.top = 48 + self.imgHeight * self.count.virtical + 'px';
        img.style.left = 10 + self.imgWidth * self.count.horizontal + 'px';

        self.gallery.appendChild(img);

      }, 150);
    }
    
  };
    
};

$('span.live').hide();
$('input.snapshot').hide();
var rtc = new Rtc();
rtc.init();

$("input.streamOn").click(function(){
  var status = $('input.streamOn').val();
  switch(status) {
   case 'On':
    rtc.streamOn();
    $('input.streamOn').removeClass('btn-danger')
      .addClass('btn-primary').attr('value', 'Off');
    $('input.snapshot').show();
    $('span.live').show();
    break;
    case 'Off':
    rtc.streamOff();
    $('input.streamOn').removeClass('btn-primary')
      .addClass('btn-danger').attr('value', 'On');
    $('input.snapshot').hide();
    $('span.live').hide();
    break;
  }
});
$("input.snapShot").click(function(){
  if( rtc.getSnapShot() != false ) {
    $("input.snapShot").hide();
  }
});

